PGDMP     &                    w            greenDB    11.4    11.4 �    .           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            /           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            0           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            1           1262    18562    greenDB    DATABASE     �   CREATE DATABASE "greenDB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "greenDB";
             postgres    false            �            1259    18563 
   auth_group    TABLE     f   CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);
    DROP TABLE public.auth_group;
       public         postgres    false            �            1259    18566    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public       postgres    false    196            2           0    0    auth_group_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;
            public       postgres    false    197            �            1259    18568    auth_group_permissions    TABLE     �   CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         postgres    false            �            1259    18571    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public       postgres    false    198            3           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;
            public       postgres    false    199            �            1259    18573    auth_permission    TABLE     �   CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         postgres    false            �            1259    18576    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public       postgres    false    200            4           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;
            public       postgres    false    201            �            1259    18578 	   auth_user    TABLE     �  CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         postgres    false            �            1259    18584    auth_user_groups    TABLE        CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         postgres    false            �            1259    18587    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public       postgres    false    203            5           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;
            public       postgres    false    204            �            1259    18589    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public       postgres    false    202            6           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;
            public       postgres    false    205            �            1259    18591    auth_user_user_permissions    TABLE     �   CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         postgres    false            �            1259    18594 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public       postgres    false    206            7           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;
            public       postgres    false    207            �            1259    18596    cmsapp_aboutus    TABLE     m  CREATE TABLE public.cmsapp_aboutus (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(100) NOT NULL,
    subtitle character varying(200) NOT NULL,
    image character varying(100) NOT NULL,
    caption character varying(100) NOT NULL,
    text text NOT NULL
);
 "   DROP TABLE public.cmsapp_aboutus;
       public         postgres    false            �            1259    18602    cmsapp_aboutus_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_aboutus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cmsapp_aboutus_id_seq;
       public       postgres    false    208            8           0    0    cmsapp_aboutus_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cmsapp_aboutus_id_seq OWNED BY public.cmsapp_aboutus.id;
            public       postgres    false    209            �            1259    18604    cmsapp_blog    TABLE     x  CREATE TABLE public.cmsapp_blog (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(150) NOT NULL,
    text text NOT NULL,
    image character varying(100) NOT NULL,
    author character varying(90) NOT NULL,
    created_by_id integer NOT NULL,
    subtle text NOT NULL
);
    DROP TABLE public.cmsapp_blog;
       public         postgres    false            �            1259    18610    cmsapp_blog_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_blog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.cmsapp_blog_id_seq;
       public       postgres    false    210            9           0    0    cmsapp_blog_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.cmsapp_blog_id_seq OWNED BY public.cmsapp_blog.id;
            public       postgres    false    211            �            1259    18612    cmsapp_event    TABLE     �  CREATE TABLE public.cmsapp_event (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(150) NOT NULL,
    image character varying(100) NOT NULL,
    content text NOT NULL,
    date_of_event date NOT NULL,
    venue character varying(300) NOT NULL,
    date timestamp with time zone NOT NULL,
    created_by_id integer NOT NULL
);
     DROP TABLE public.cmsapp_event;
       public         postgres    false            �            1259    18618    cmsapp_event_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cmsapp_event_id_seq;
       public       postgres    false    212            :           0    0    cmsapp_event_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cmsapp_event_id_seq OWNED BY public.cmsapp_event.id;
            public       postgres    false    213            �            1259    18620    cmsapp_gallery    TABLE     ~  CREATE TABLE public.cmsapp_gallery (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(100) NOT NULL,
    caption character varying(200) NOT NULL,
    image character varying(100) NOT NULL,
    published boolean NOT NULL,
    priority integer,
    menubar_id integer NOT NULL
);
 "   DROP TABLE public.cmsapp_gallery;
       public         postgres    false            �            1259    18623    cmsapp_gallery_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_gallery_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cmsapp_gallery_id_seq;
       public       postgres    false    214            ;           0    0    cmsapp_gallery_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cmsapp_gallery_id_seq OWNED BY public.cmsapp_gallery.id;
            public       postgres    false    215            �            1259    18625    cmsapp_menubar    TABLE     �  CREATE TABLE public.cmsapp_menubar (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(150) NOT NULL,
    slug character varying(50),
    link character varying(100),
    icon character varying(20),
    priority integer,
    published boolean NOT NULL,
    menu_type_id integer,
    root_id integer
);
 "   DROP TABLE public.cmsapp_menubar;
       public         postgres    false            �            1259    18628    cmsapp_menubar_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_menubar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cmsapp_menubar_id_seq;
       public       postgres    false    216            <           0    0    cmsapp_menubar_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cmsapp_menubar_id_seq OWNED BY public.cmsapp_menubar.id;
            public       postgres    false    217            �            1259    18630    cmsapp_menucat    TABLE     �   CREATE TABLE public.cmsapp_menucat (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(100) NOT NULL,
    text text,
    published boolean NOT NULL
);
 "   DROP TABLE public.cmsapp_menucat;
       public         postgres    false            �            1259    18636    cmsapp_menucat_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_menucat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cmsapp_menucat_id_seq;
       public       postgres    false    218            =           0    0    cmsapp_menucat_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cmsapp_menucat_id_seq OWNED BY public.cmsapp_menucat.id;
            public       postgres    false    219            �            1259    18638    cmsapp_message    TABLE     �  CREATE TABLE public.cmsapp_message (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    sender character varying(50) NOT NULL,
    mobile character varying(50) NOT NULL,
    email character varying(254),
    subject character varying(220) NOT NULL,
    message text NOT NULL,
    date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.cmsapp_message;
       public         postgres    false            �            1259    18644    cmsapp_message_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cmsapp_message_id_seq;
       public       postgres    false    220            >           0    0    cmsapp_message_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cmsapp_message_id_seq OWNED BY public.cmsapp_message.id;
            public       postgres    false    221            �            1259    18646    cmsapp_notice    TABLE     9  CREATE TABLE public.cmsapp_notice (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(100) NOT NULL,
    content text NOT NULL,
    date timestamp with time zone NOT NULL,
    created_by_id integer NOT NULL
);
 !   DROP TABLE public.cmsapp_notice;
       public         postgres    false            �            1259    18652    cmsapp_notice_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_notice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.cmsapp_notice_id_seq;
       public       postgres    false    222            ?           0    0    cmsapp_notice_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.cmsapp_notice_id_seq OWNED BY public.cmsapp_notice.id;
            public       postgres    false    223            �            1259    18654    cmsapp_overview    TABLE     �  CREATE TABLE public.cmsapp_overview (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(200) NOT NULL,
    subtitle character varying(200),
    subtle text,
    priority integer,
    image character varying(100),
    caption character varying(150),
    text text NOT NULL,
    published boolean NOT NULL,
    menubar_id integer NOT NULL
);
 #   DROP TABLE public.cmsapp_overview;
       public         postgres    false            �            1259    18660    cmsapp_overview_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_overview_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.cmsapp_overview_id_seq;
       public       postgres    false    224            @           0    0    cmsapp_overview_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.cmsapp_overview_id_seq OWNED BY public.cmsapp_overview.id;
            public       postgres    false    225            �            1259    18662    cmsapp_page    TABLE     �  CREATE TABLE public.cmsapp_page (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(200) NOT NULL,
    subtitle character varying(200),
    subtle text,
    priority integer,
    image character varying(100),
    caption character varying(150),
    text text NOT NULL,
    published boolean NOT NULL,
    category_id integer NOT NULL
);
    DROP TABLE public.cmsapp_page;
       public         postgres    false            �            1259    18668    cmsapp_page_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.cmsapp_page_id_seq;
       public       postgres    false    226            A           0    0    cmsapp_page_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.cmsapp_page_id_seq OWNED BY public.cmsapp_page.id;
            public       postgres    false    227            �            1259    18670    cmsapp_projectarea    TABLE     �  CREATE TABLE public.cmsapp_projectarea (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(200) NOT NULL,
    subtitle character varying(200),
    subtle text,
    subtle_limit integer,
    priority integer,
    image character varying(100) NOT NULL,
    caption character varying(150),
    text text NOT NULL
);
 &   DROP TABLE public.cmsapp_projectarea;
       public         postgres    false            �            1259    18676    cmsapp_projectarea_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_projectarea_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.cmsapp_projectarea_id_seq;
       public       postgres    false    228            B           0    0    cmsapp_projectarea_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.cmsapp_projectarea_id_seq OWNED BY public.cmsapp_projectarea.id;
            public       postgres    false    229            �            1259    18678    cmsapp_slideshow    TABLE     T  CREATE TABLE public.cmsapp_slideshow (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title character varying(90) NOT NULL,
    caption character varying(90),
    priority integer,
    image character varying(100) NOT NULL,
    published boolean NOT NULL
);
 $   DROP TABLE public.cmsapp_slideshow;
       public         postgres    false            �            1259    18681    cmsapp_slideshow_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cmsapp_slideshow_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.cmsapp_slideshow_id_seq;
       public       postgres    false    230            C           0    0    cmsapp_slideshow_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.cmsapp_slideshow_id_seq OWNED BY public.cmsapp_slideshow.id;
            public       postgres    false    231            �            1259    18683    django_admin_log    TABLE     �  CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         postgres    false            �            1259    18690    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public       postgres    false    232            D           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;
            public       postgres    false    233            �            1259    18692    django_content_type    TABLE     �   CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         postgres    false            �            1259    18695    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public       postgres    false    234            E           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;
            public       postgres    false    235            �            1259    18697    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         postgres    false            �            1259    18703    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public       postgres    false    236            F           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
            public       postgres    false    237            �            1259    18705    django_session    TABLE     �   CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         postgres    false            �            1259    18711    django_summernote_attachment    TABLE     �   CREATE TABLE public.django_summernote_attachment (
    id integer NOT NULL,
    name character varying(255),
    file character varying(100) NOT NULL,
    uploaded timestamp with time zone NOT NULL
);
 0   DROP TABLE public.django_summernote_attachment;
       public         postgres    false            �            1259    18714 #   django_summernote_attachment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_summernote_attachment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 :   DROP SEQUENCE public.django_summernote_attachment_id_seq;
       public       postgres    false    239            G           0    0 #   django_summernote_attachment_id_seq    SEQUENCE OWNED BY     k   ALTER SEQUENCE public.django_summernote_attachment_id_seq OWNED BY public.django_summernote_attachment.id;
            public       postgres    false    240                       2604    18716    auth_group id    DEFAULT     n   ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196                       2604    18717    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198                       2604    18718    auth_permission id    DEFAULT     x   ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    201    200                       2604    18719    auth_user id    DEFAULT     l   ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    202                       2604    18720    auth_user_groups id    DEFAULT     z   ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    203                       2604    18721    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206                       2604    18722    cmsapp_aboutus id    DEFAULT     v   ALTER TABLE ONLY public.cmsapp_aboutus ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_aboutus_id_seq'::regclass);
 @   ALTER TABLE public.cmsapp_aboutus ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    209    208                       2604    18723    cmsapp_blog id    DEFAULT     p   ALTER TABLE ONLY public.cmsapp_blog ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_blog_id_seq'::regclass);
 =   ALTER TABLE public.cmsapp_blog ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    211    210                       2604    18724    cmsapp_event id    DEFAULT     r   ALTER TABLE ONLY public.cmsapp_event ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_event_id_seq'::regclass);
 >   ALTER TABLE public.cmsapp_event ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    213    212                       2604    18725    cmsapp_gallery id    DEFAULT     v   ALTER TABLE ONLY public.cmsapp_gallery ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_gallery_id_seq'::regclass);
 @   ALTER TABLE public.cmsapp_gallery ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    215    214                       2604    18726    cmsapp_menubar id    DEFAULT     v   ALTER TABLE ONLY public.cmsapp_menubar ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_menubar_id_seq'::regclass);
 @   ALTER TABLE public.cmsapp_menubar ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    217    216                       2604    18727    cmsapp_menucat id    DEFAULT     v   ALTER TABLE ONLY public.cmsapp_menucat ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_menucat_id_seq'::regclass);
 @   ALTER TABLE public.cmsapp_menucat ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    219    218                       2604    18728    cmsapp_message id    DEFAULT     v   ALTER TABLE ONLY public.cmsapp_message ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_message_id_seq'::regclass);
 @   ALTER TABLE public.cmsapp_message ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    221    220                       2604    18729    cmsapp_notice id    DEFAULT     t   ALTER TABLE ONLY public.cmsapp_notice ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_notice_id_seq'::regclass);
 ?   ALTER TABLE public.cmsapp_notice ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    223    222                       2604    18730    cmsapp_overview id    DEFAULT     x   ALTER TABLE ONLY public.cmsapp_overview ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_overview_id_seq'::regclass);
 A   ALTER TABLE public.cmsapp_overview ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    225    224                       2604    18731    cmsapp_page id    DEFAULT     p   ALTER TABLE ONLY public.cmsapp_page ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_page_id_seq'::regclass);
 =   ALTER TABLE public.cmsapp_page ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    227    226                       2604    18732    cmsapp_projectarea id    DEFAULT     ~   ALTER TABLE ONLY public.cmsapp_projectarea ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_projectarea_id_seq'::regclass);
 D   ALTER TABLE public.cmsapp_projectarea ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    229    228                       2604    18733    cmsapp_slideshow id    DEFAULT     z   ALTER TABLE ONLY public.cmsapp_slideshow ALTER COLUMN id SET DEFAULT nextval('public.cmsapp_slideshow_id_seq'::regclass);
 B   ALTER TABLE public.cmsapp_slideshow ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    230                       2604    18734    django_admin_log id    DEFAULT     z   ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    233    232                        2604    18735    django_content_type id    DEFAULT     �   ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    235    234            !           2604    18736    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    237    236            "           2604    18737    django_summernote_attachment id    DEFAULT     �   ALTER TABLE ONLY public.django_summernote_attachment ALTER COLUMN id SET DEFAULT nextval('public.django_summernote_attachment_id_seq'::regclass);
 N   ALTER TABLE public.django_summernote_attachment ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    240    239            �          0    18563 
   auth_group 
   TABLE DATA               .   COPY public.auth_group (id, name) FROM stdin;
    public       postgres    false    196   z                0    18568    auth_group_permissions 
   TABLE DATA               M   COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public       postgres    false    198   �                0    18573    auth_permission 
   TABLE DATA               N   COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
    public       postgres    false    200   �                0    18578 	   auth_user 
   TABLE DATA               �   COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public       postgres    false    202   �                0    18584    auth_user_groups 
   TABLE DATA               A   COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
    public       postgres    false    203   5      	          0    18591    auth_user_user_permissions 
   TABLE DATA               P   COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public       postgres    false    206   R                0    18596    cmsapp_aboutus 
   TABLE DATA               k   COPY public.cmsapp_aboutus (id, created_at, updated_at, title, subtitle, image, caption, text) FROM stdin;
    public       postgres    false    208   o                0    18604    cmsapp_blog 
   TABLE DATA               t   COPY public.cmsapp_blog (id, created_at, updated_at, title, text, image, author, created_by_id, subtle) FROM stdin;
    public       postgres    false    210   >                0    18612    cmsapp_event 
   TABLE DATA               �   COPY public.cmsapp_event (id, created_at, updated_at, title, image, content, date_of_event, venue, date, created_by_id) FROM stdin;
    public       postgres    false    212   :@                0    18620    cmsapp_gallery 
   TABLE DATA               |   COPY public.cmsapp_gallery (id, created_at, updated_at, title, caption, image, published, priority, menubar_id) FROM stdin;
    public       postgres    false    214   W@                0    18625    cmsapp_menubar 
   TABLE DATA               �   COPY public.cmsapp_menubar (id, created_at, updated_at, title, slug, link, icon, priority, published, menu_type_id, root_id) FROM stdin;
    public       postgres    false    216   2B                0    18630    cmsapp_menucat 
   TABLE DATA               \   COPY public.cmsapp_menucat (id, created_at, updated_at, title, text, published) FROM stdin;
    public       postgres    false    218   D                0    18638    cmsapp_message 
   TABLE DATA               s   COPY public.cmsapp_message (id, created_at, updated_at, sender, mobile, email, subject, message, date) FROM stdin;
    public       postgres    false    220   0D                0    18646    cmsapp_notice 
   TABLE DATA               h   COPY public.cmsapp_notice (id, created_at, updated_at, title, content, date, created_by_id) FROM stdin;
    public       postgres    false    222   MD                0    18654    cmsapp_overview 
   TABLE DATA               �   COPY public.cmsapp_overview (id, created_at, updated_at, title, subtitle, subtle, priority, image, caption, text, published, menubar_id) FROM stdin;
    public       postgres    false    224   jD                0    18662    cmsapp_page 
   TABLE DATA               �   COPY public.cmsapp_page (id, created_at, updated_at, title, subtitle, subtle, priority, image, caption, text, published, category_id) FROM stdin;
    public       postgres    false    226   �J                0    18670    cmsapp_projectarea 
   TABLE DATA               �   COPY public.cmsapp_projectarea (id, created_at, updated_at, title, subtitle, subtle, subtle_limit, priority, image, caption, text) FROM stdin;
    public       postgres    false    228   %N      !          0    18678    cmsapp_slideshow 
   TABLE DATA               r   COPY public.cmsapp_slideshow (id, created_at, updated_at, title, caption, priority, image, published) FROM stdin;
    public       postgres    false    230   �P      #          0    18683    django_admin_log 
   TABLE DATA               �   COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    public       postgres    false    232   QQ      %          0    18692    django_content_type 
   TABLE DATA               C   COPY public.django_content_type (id, app_label, model) FROM stdin;
    public       postgres    false    234   nQ      '          0    18697    django_migrations 
   TABLE DATA               C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public       postgres    false    236   GR      )          0    18705    django_session 
   TABLE DATA               P   COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
    public       postgres    false    238   �U      *          0    18711    django_summernote_attachment 
   TABLE DATA               P   COPY public.django_summernote_attachment (id, name, file, uploaded) FROM stdin;
    public       postgres    false    239   �W      H           0    0    auth_group_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);
            public       postgres    false    197            I           0    0    auth_group_permissions_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);
            public       postgres    false    199            J           0    0    auth_permission_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.auth_permission_id_seq', 76, true);
            public       postgres    false    201            K           0    0    auth_user_groups_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);
            public       postgres    false    204            L           0    0    auth_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);
            public       postgres    false    205            M           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);
            public       postgres    false    207            N           0    0    cmsapp_aboutus_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cmsapp_aboutus_id_seq', 2, true);
            public       postgres    false    209            O           0    0    cmsapp_blog_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.cmsapp_blog_id_seq', 5, true);
            public       postgres    false    211            P           0    0    cmsapp_event_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cmsapp_event_id_seq', 1, false);
            public       postgres    false    213            Q           0    0    cmsapp_gallery_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cmsapp_gallery_id_seq', 4, true);
            public       postgres    false    215            R           0    0    cmsapp_menubar_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cmsapp_menubar_id_seq', 13, true);
            public       postgres    false    217            S           0    0    cmsapp_menucat_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cmsapp_menucat_id_seq', 1, false);
            public       postgres    false    219            T           0    0    cmsapp_message_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cmsapp_message_id_seq', 1, false);
            public       postgres    false    221            U           0    0    cmsapp_notice_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.cmsapp_notice_id_seq', 1, false);
            public       postgres    false    223            V           0    0    cmsapp_overview_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cmsapp_overview_id_seq', 3, true);
            public       postgres    false    225            W           0    0    cmsapp_page_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.cmsapp_page_id_seq', 6, true);
            public       postgres    false    227            X           0    0    cmsapp_projectarea_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.cmsapp_projectarea_id_seq', 1, true);
            public       postgres    false    229            Y           0    0    cmsapp_slideshow_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.cmsapp_slideshow_id_seq', 1, true);
            public       postgres    false    231            Z           0    0    django_admin_log_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);
            public       postgres    false    233            [           0    0    django_content_type_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.django_content_type_id_seq', 19, true);
            public       postgres    false    235            \           0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 43, true);
            public       postgres    false    237            ]           0    0 #   django_summernote_attachment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.django_summernote_attachment_id_seq', 1, true);
            public       postgres    false    240            %           2606    18745    auth_group auth_group_name_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public         postgres    false    196            *           2606    18747 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 |   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       public         postgres    false    198    198            -           2606    18749 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public         postgres    false    198            '           2606    18751    auth_group auth_group_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public         postgres    false    196            0           2606    18753 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 p   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       public         postgres    false    200    200            2           2606    18755 $   auth_permission auth_permission_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public         postgres    false    200            :           2606    18757 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public         postgres    false    203            =           2606    18759 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 j   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       public         postgres    false    203    203            4           2606    18761    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public         postgres    false    202            @           2606    18763 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public         postgres    false    206            C           2606    18765 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       public         postgres    false    206    206            7           2606    18767     auth_user auth_user_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public         postgres    false    202            E           2606    18769 "   cmsapp_aboutus cmsapp_aboutus_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cmsapp_aboutus
    ADD CONSTRAINT cmsapp_aboutus_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cmsapp_aboutus DROP CONSTRAINT cmsapp_aboutus_pkey;
       public         postgres    false    208            H           2606    18771    cmsapp_blog cmsapp_blog_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.cmsapp_blog
    ADD CONSTRAINT cmsapp_blog_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.cmsapp_blog DROP CONSTRAINT cmsapp_blog_pkey;
       public         postgres    false    210            K           2606    18773    cmsapp_event cmsapp_event_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cmsapp_event
    ADD CONSTRAINT cmsapp_event_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cmsapp_event DROP CONSTRAINT cmsapp_event_pkey;
       public         postgres    false    212            N           2606    18775 "   cmsapp_gallery cmsapp_gallery_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cmsapp_gallery
    ADD CONSTRAINT cmsapp_gallery_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cmsapp_gallery DROP CONSTRAINT cmsapp_gallery_pkey;
       public         postgres    false    214            Q           2606    18777 "   cmsapp_menubar cmsapp_menubar_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cmsapp_menubar
    ADD CONSTRAINT cmsapp_menubar_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cmsapp_menubar DROP CONSTRAINT cmsapp_menubar_pkey;
       public         postgres    false    216            T           2606    18779 "   cmsapp_menucat cmsapp_menucat_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cmsapp_menucat
    ADD CONSTRAINT cmsapp_menucat_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cmsapp_menucat DROP CONSTRAINT cmsapp_menucat_pkey;
       public         postgres    false    218            V           2606    18781 "   cmsapp_message cmsapp_message_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cmsapp_message
    ADD CONSTRAINT cmsapp_message_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cmsapp_message DROP CONSTRAINT cmsapp_message_pkey;
       public         postgres    false    220            Y           2606    18783     cmsapp_notice cmsapp_notice_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.cmsapp_notice
    ADD CONSTRAINT cmsapp_notice_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.cmsapp_notice DROP CONSTRAINT cmsapp_notice_pkey;
       public         postgres    false    222            [           2606    18785 8   cmsapp_overview cmsapp_overview_menubar_id_85fa4ee6_uniq 
   CONSTRAINT     y   ALTER TABLE ONLY public.cmsapp_overview
    ADD CONSTRAINT cmsapp_overview_menubar_id_85fa4ee6_uniq UNIQUE (menubar_id);
 b   ALTER TABLE ONLY public.cmsapp_overview DROP CONSTRAINT cmsapp_overview_menubar_id_85fa4ee6_uniq;
       public         postgres    false    224            ]           2606    18787 $   cmsapp_overview cmsapp_overview_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.cmsapp_overview
    ADD CONSTRAINT cmsapp_overview_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.cmsapp_overview DROP CONSTRAINT cmsapp_overview_pkey;
       public         postgres    false    224            `           2606    18789    cmsapp_page cmsapp_page_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.cmsapp_page
    ADD CONSTRAINT cmsapp_page_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.cmsapp_page DROP CONSTRAINT cmsapp_page_pkey;
       public         postgres    false    226            b           2606    18791 *   cmsapp_projectarea cmsapp_projectarea_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.cmsapp_projectarea
    ADD CONSTRAINT cmsapp_projectarea_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.cmsapp_projectarea DROP CONSTRAINT cmsapp_projectarea_pkey;
       public         postgres    false    228            d           2606    18793 &   cmsapp_slideshow cmsapp_slideshow_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.cmsapp_slideshow
    ADD CONSTRAINT cmsapp_slideshow_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.cmsapp_slideshow DROP CONSTRAINT cmsapp_slideshow_pkey;
       public         postgres    false    230            g           2606    18795 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public         postgres    false    232            j           2606    18797 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 o   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       public         postgres    false    234    234            l           2606    18799 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public         postgres    false    234            n           2606    18801 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public         postgres    false    236            q           2606    18803 "   django_session django_session_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public         postgres    false    238            t           2606    18805 >   django_summernote_attachment django_summernote_attachment_pkey 
   CONSTRAINT     |   ALTER TABLE ONLY public.django_summernote_attachment
    ADD CONSTRAINT django_summernote_attachment_pkey PRIMARY KEY (id);
 h   ALTER TABLE ONLY public.django_summernote_attachment DROP CONSTRAINT django_summernote_attachment_pkey;
       public         postgres    false    239            #           1259    18806    auth_group_name_a6ea08ec_like    INDEX     h   CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);
 1   DROP INDEX public.auth_group_name_a6ea08ec_like;
       public         postgres    false    196            (           1259    18807 (   auth_group_permissions_group_id_b120cbf9    INDEX     o   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);
 <   DROP INDEX public.auth_group_permissions_group_id_b120cbf9;
       public         postgres    false    198            +           1259    18808 -   auth_group_permissions_permission_id_84c5c92e    INDEX     y   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);
 A   DROP INDEX public.auth_group_permissions_permission_id_84c5c92e;
       public         postgres    false    198            .           1259    18809 (   auth_permission_content_type_id_2f476e4b    INDEX     o   CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);
 <   DROP INDEX public.auth_permission_content_type_id_2f476e4b;
       public         postgres    false    200            8           1259    18810 "   auth_user_groups_group_id_97559544    INDEX     c   CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);
 6   DROP INDEX public.auth_user_groups_group_id_97559544;
       public         postgres    false    203            ;           1259    18811 !   auth_user_groups_user_id_6a12ed8b    INDEX     a   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);
 5   DROP INDEX public.auth_user_groups_user_id_6a12ed8b;
       public         postgres    false    203            >           1259    18812 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);
 E   DROP INDEX public.auth_user_user_permissions_permission_id_1fbb5f2c;
       public         postgres    false    206            A           1259    18813 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     u   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);
 ?   DROP INDEX public.auth_user_user_permissions_user_id_a95ead1b;
       public         postgres    false    206            5           1259    18814     auth_user_username_6821ab7c_like    INDEX     n   CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);
 4   DROP INDEX public.auth_user_username_6821ab7c_like;
       public         postgres    false    202            F           1259    18815 "   cmsapp_blog_created_by_id_4e37cea1    INDEX     c   CREATE INDEX cmsapp_blog_created_by_id_4e37cea1 ON public.cmsapp_blog USING btree (created_by_id);
 6   DROP INDEX public.cmsapp_blog_created_by_id_4e37cea1;
       public         postgres    false    210            I           1259    18816 #   cmsapp_event_created_by_id_6a45eb3b    INDEX     e   CREATE INDEX cmsapp_event_created_by_id_6a45eb3b ON public.cmsapp_event USING btree (created_by_id);
 7   DROP INDEX public.cmsapp_event_created_by_id_6a45eb3b;
       public         postgres    false    212            L           1259    18817 "   cmsapp_gallery_menubar_id_68e78422    INDEX     c   CREATE INDEX cmsapp_gallery_menubar_id_68e78422 ON public.cmsapp_gallery USING btree (menubar_id);
 6   DROP INDEX public.cmsapp_gallery_menubar_id_68e78422;
       public         postgres    false    214            O           1259    18818 $   cmsapp_menubar_menu_type_id_e6cf1fc5    INDEX     g   CREATE INDEX cmsapp_menubar_menu_type_id_e6cf1fc5 ON public.cmsapp_menubar USING btree (menu_type_id);
 8   DROP INDEX public.cmsapp_menubar_menu_type_id_e6cf1fc5;
       public         postgres    false    216            R           1259    18819    cmsapp_menubar_root_id_5c19eeb2    INDEX     ]   CREATE INDEX cmsapp_menubar_root_id_5c19eeb2 ON public.cmsapp_menubar USING btree (root_id);
 3   DROP INDEX public.cmsapp_menubar_root_id_5c19eeb2;
       public         postgres    false    216            W           1259    18820 $   cmsapp_notice_created_by_id_f38f7af4    INDEX     g   CREATE INDEX cmsapp_notice_created_by_id_f38f7af4 ON public.cmsapp_notice USING btree (created_by_id);
 8   DROP INDEX public.cmsapp_notice_created_by_id_f38f7af4;
       public         postgres    false    222            ^           1259    18821     cmsapp_page_category_id_a8d61ed7    INDEX     _   CREATE INDEX cmsapp_page_category_id_a8d61ed7 ON public.cmsapp_page USING btree (category_id);
 4   DROP INDEX public.cmsapp_page_category_id_a8d61ed7;
       public         postgres    false    226            e           1259    18822 )   django_admin_log_content_type_id_c4bce8eb    INDEX     q   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);
 =   DROP INDEX public.django_admin_log_content_type_id_c4bce8eb;
       public         postgres    false    232            h           1259    18823 !   django_admin_log_user_id_c564eba6    INDEX     a   CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);
 5   DROP INDEX public.django_admin_log_user_id_c564eba6;
       public         postgres    false    232            o           1259    18824 #   django_session_expire_date_a5c62663    INDEX     e   CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);
 7   DROP INDEX public.django_session_expire_date_a5c62663;
       public         postgres    false    238            r           1259    18825 (   django_session_session_key_c0390e0f_like    INDEX     ~   CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);
 <   DROP INDEX public.django_session_session_key_c0390e0f_like;
       public         postgres    false    238            u           2606    18826 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 y   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       public       postgres    false    198    2866    200            v           2606    18831 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       public       postgres    false    198    2855    196            w           2606    18836 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       public       postgres    false    2924    234    200            x           2606    18841 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       public       postgres    false    203    2855    196            y           2606    18846 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       public       postgres    false    202    2868    203            z           2606    18851 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 }   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       public       postgres    false    206    2866    200            {           2606    18856 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       public       postgres    false    2868    202    206            |           2606    18861 >   cmsapp_blog cmsapp_blog_created_by_id_4e37cea1_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_blog
    ADD CONSTRAINT cmsapp_blog_created_by_id_4e37cea1_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 h   ALTER TABLE ONLY public.cmsapp_blog DROP CONSTRAINT cmsapp_blog_created_by_id_4e37cea1_fk_auth_user_id;
       public       postgres    false    210    2868    202            }           2606    18866 @   cmsapp_event cmsapp_event_created_by_id_6a45eb3b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_event
    ADD CONSTRAINT cmsapp_event_created_by_id_6a45eb3b_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.cmsapp_event DROP CONSTRAINT cmsapp_event_created_by_id_6a45eb3b_fk_auth_user_id;
       public       postgres    false    2868    202    212            ~           2606    18871 F   cmsapp_gallery cmsapp_gallery_menubar_id_68e78422_fk_cmsapp_menubar_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_gallery
    ADD CONSTRAINT cmsapp_gallery_menubar_id_68e78422_fk_cmsapp_menubar_id FOREIGN KEY (menubar_id) REFERENCES public.cmsapp_menubar(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY public.cmsapp_gallery DROP CONSTRAINT cmsapp_gallery_menubar_id_68e78422_fk_cmsapp_menubar_id;
       public       postgres    false    2897    214    216                       2606    18876 H   cmsapp_menubar cmsapp_menubar_menu_type_id_e6cf1fc5_fk_cmsapp_menucat_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_menubar
    ADD CONSTRAINT cmsapp_menubar_menu_type_id_e6cf1fc5_fk_cmsapp_menucat_id FOREIGN KEY (menu_type_id) REFERENCES public.cmsapp_menucat(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY public.cmsapp_menubar DROP CONSTRAINT cmsapp_menubar_menu_type_id_e6cf1fc5_fk_cmsapp_menucat_id;
       public       postgres    false    2900    216    218            �           2606    18881 C   cmsapp_menubar cmsapp_menubar_root_id_5c19eeb2_fk_cmsapp_menubar_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_menubar
    ADD CONSTRAINT cmsapp_menubar_root_id_5c19eeb2_fk_cmsapp_menubar_id FOREIGN KEY (root_id) REFERENCES public.cmsapp_menubar(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY public.cmsapp_menubar DROP CONSTRAINT cmsapp_menubar_root_id_5c19eeb2_fk_cmsapp_menubar_id;
       public       postgres    false    216    2897    216            �           2606    18886 B   cmsapp_notice cmsapp_notice_created_by_id_f38f7af4_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_notice
    ADD CONSTRAINT cmsapp_notice_created_by_id_f38f7af4_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.cmsapp_notice DROP CONSTRAINT cmsapp_notice_created_by_id_f38f7af4_fk_auth_user_id;
       public       postgres    false    222    202    2868            �           2606    18891 H   cmsapp_overview cmsapp_overview_menubar_id_85fa4ee6_fk_cmsapp_menubar_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_overview
    ADD CONSTRAINT cmsapp_overview_menubar_id_85fa4ee6_fk_cmsapp_menubar_id FOREIGN KEY (menubar_id) REFERENCES public.cmsapp_menubar(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY public.cmsapp_overview DROP CONSTRAINT cmsapp_overview_menubar_id_85fa4ee6_fk_cmsapp_menubar_id;
       public       postgres    false    224    216    2897            �           2606    18896 A   cmsapp_page cmsapp_page_category_id_a8d61ed7_fk_cmsapp_menubar_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.cmsapp_page
    ADD CONSTRAINT cmsapp_page_category_id_a8d61ed7_fk_cmsapp_menubar_id FOREIGN KEY (category_id) REFERENCES public.cmsapp_menubar(id) DEFERRABLE INITIALLY DEFERRED;
 k   ALTER TABLE ONLY public.cmsapp_page DROP CONSTRAINT cmsapp_page_category_id_a8d61ed7_fk_cmsapp_menubar_id;
       public       postgres    false    2897    216    226            �           2606    18901 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 q   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       public       postgres    false    2924    234    232            �           2606    18906 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       public       postgres    false    2868    232    202            �      x������ � �            x������ � �         �  x�]�[��0E��U��Q�3��m�4r����@$���@���a�A>�[�}š��K��E�������kX����k2T���i�`���sO?�6j�앦�}����JP��j9��b;�gXT�Q	��;��u�/W� \�p�rTG/D�v"u)E� ��r��9M���҇Z��U<>�><<���e8:;>��e}k���J���v|8j��B�?i��~�/���Aװ����j¸�ɻ0�ɘI�`�ǘʺ@`ň$=����]F��	�0hĘɷ`hĘ��@`Ĉ�����;��v�+�i��HL�@��<c�EI�3�s��	Ǖ�L��A�L�C�T�F:cT�qqkc�Ӻ��q�D���"Pym`}�^��@A��s�&���ץc�����s��\���B+d�ܞ���/E�RtGw�۶|m������o�|wq}��;<OrK�����6�m�q!Ot��6|c:��4#6�`a��ΰh�J���䡿]��4��aaz�zz@���z~\P��?i�
.�Okߘ���izB�h�d��Bs`&�t�0.�~���=�|kzB�"�t�P�D��a���k|<�����;.}k�@A��@�ɾ��g�8yE�?b$��~]��3g }�1�f]�Qqf<�.)�����<�e�w&��O��M�-M_BE��[6����{��й_         �   x�=�A�0@�ۯ��M��s�}�%XbA�� ���2%��	A�]��dl����n@F.�l��ZQھ�6�xr�|T�^�m���yVd;<Tv�W�:�mgL���Ê ���� Ӏ "zLj!�LFkޏY���tx��ҡ
�X��@���$v)�            x������ � �      	      x������ � �         �	  x��Y�r�6>�O��T%v��#ɊƊ��-{Te�*K���	��@�@��"���}9?�~���W�u�#;q�������Gk��hg}��>~������d�mn��6�2ܚln��	�3];�֮��3'�LZ�#�8���@$u̝�e�����r�(x&�3^�h7�E���[1�%~���պ��e�M�,1C�',�J�	3���֨���GOX�K���B�f¦B]
'c�g�Fr�g��v�BD�N�`1a�Hd]<���ڊ�wn�Wͅ�r7a3�a��S=S��Ǩ3�H�����VO���0k7}�-+x������p8d�4ʲ��9��ֱ�g�N��W�XT��"gx���B$��U&�R�/�m��Il.]�7�z5i��i��wz��Tm0s�ЌJ[L�)7�V������B "��L��/�]&63�(ۧ�s�W.����e�U�R`vM����,�fq.U�ر���u�1%���p1�k�HO�P�K����ޟ�o�z�b�t9y�*Q>��7/�,��O��\��|�K���,d�#��9 �	?�O������9
�K�j�ͽÑ�boq Y����
��.�T@��7u�F�0�ä�$�LL���%��p�����vl ��r��Y"l%w~�����J[���P�4hX�	ƌ�J��?��:�#(	�9onr����H/�ṉ�Q]r��2��]a���Z&�S\ƺ�q��3:2��>|ť(��>~��:]U�	�6�~g�ޠ�;�L� !ta=�{4m�|?��Kn0�J�R;���?3kY��>~�W�ȵa�q�қ��������(��Qω��/�D��Uj��wg�&�|��ٛ���Y�(�Lr	�W�$�m�o6�^���.:.�k�\k巸�
��g?�C�}[����^�����O�4��F:��/	�Mγ�1�
���%⥱��nN���ؑ�bV�`��UBW�n@����N@✑B��)�%ڧ'�R����bc2�DA�l�~�ڹ���u�K#�uST�7�� ����lW�����0�'(B��Z�+PF#_������|�����;�t��u����@ȴ���>��>�����{r&)�9{�Q��O��6G��kH�*�'%�M�Rz�%�2�w�k�R2��q��jQ���!~������Κ��1D��cI."�����,e8K����vgfow������RLK��ldWw��6?g�[��r*�51�r	o���l�� ������X�+_ѵ��*o�ݼ�~K1�kJ���<1�F�D_uO�g��S!|�O:d�k��S�*���Y���Xk��+��NF;;�ױmG���x��?���	��Y�3��z{�%�w&�"(ii5A�J�8�M�7�؈+-
�3�g���!K.��.l�	˝�&��,S�Q�k����>1��+�]�z{G��s�e��\b9?�z�A��q��)䂶�z�����E��^]Q>�ѳ_"���cb+�;7�D�����4�ԉ�MǢ��#���,Q�ṋ�$?8�Ơ��<<֗lg�ȳU�"9�K�P��\׶���j�B7��XֿH�:Tt |�ޥ���6=�DO��Z��],m|K�)/�xM/��N���DӃW'ώN#z����G���M&�刑Xr�NB��p�cݖMHaT���U�T=̫�#�#��bR�����>Q���(V�S��|���g�W��#���Hy��$�G �Ua+m�^���P�*��+� ւ��@4^_��8�^j��z|���F����7������|6=<�+w��1�wcO�]a��{"��"d���_��\�*�;J���5�V(�!t�wL4�ب�o�Ӎz�	�[��wh��z��,��-���ѧ��pt�w0���H�	�"fumbq?`E�j�v�2f�ۨPJ(f�f5�P�]�u�I!�!�$�s�Ͳ_L����`}��h����IVo�-��P��6s$�8�%�o���7]�^�?����$k[��ʲ�ps�.8;<{u�6^=�
��;�M{tm�֋�N���p���+q){�:u�n�p:z��;
�DT���_�i��_q���	xo,��m:�?2��
�r���"��r���q+h������R�8B|�`���:OGѽ��W���덉|>Kۙ��o�Q�1�R#��U��y���������[�C���`��Ѳ��z
'�J��c&��mOf�������g������2���X&�90��}bFv.�dYƪN�~>y�Ƞ�� ��%�ii���xȱuI95|>��������;[����OI|o����]i�}����
�$h��3���P^s�nM|��Э�R�.\��g��L��R�K��%H��\⮅�#�v}N�^���J�o��I�
���i��|h���EU�UŅ���h床)��5D��������T�t�kE>I�MT���+7���O�<x�oD�            x��=�nIv��p�i.+k!�����FR�,�#�#�U]�u.,U��O���0�1'��0���G�S�^,��UE���1�LZ͌̌ʷċ�^���~Ϛ�������79�[�#�;���z�?荎�#��<����u��^%l����|�b�v/&i������&s�gQ|Lz��54��,
�Q��l�c|�e�0����%N�G�1I�����C�{w�,
����:&)��^���ypo�@|ý�郟��4��{�/a��;^������K��1s9�F�� ��k��i8��퐌g�'���6��^�3�o�Rg1O�<t�y /�0
Ye<�R��(DH��W��;��N�bF���h�Q�Xx��(��������A~�>8�8���4(��2��:JC��wN{w�<�?Fw�Gz����T���!�O���{��e�����O��W�����w�}��7[)��w�� 4�k@#1� tM���O�~a[��5)0
#D�4��X���x����hV��% B|���",� ��o���"��^�D�w`:�0?}�3���>����o9�%��D3R��ν4�	w�0��ڷ����%,tJ�R��]��܉\�?�.��ȧ`��l�����2�������o�nY��Ⱦ��O#������Ʋ�\��<V�O	�������#g�܂��@py�`3��c�/�E�:�L���~-̞Ey��SN,�!"����Z�b�<�@�+� �(��T�y�b���tE���H��Z �X�i��<����R�$KQ��E�Ѝ��6�`C�_�@�r�1�u�����d��x�J�f 2tŹ/6`�}��;$���p�!\��p��`?�KDK���|O=���̎f����Ԟ�l�0z�4���#���+�[�'�d���T@�J�+Dt7�8a�����@R����92��-k��������+h�(s$�%�y<,+u�|
@Ki%�' #�Q���6C7UW�G� t�)*��(l
�H���V�_O�:U����yb͡�c���@q��/Am�G������E@v-�w��P1��
� =X$�qm�>����l�����$��$
PE�^���7��,\�X�n�e�,!����ɉG�:^�hZ��B�]LR ��U��ڀT�����\C/�[i��p^��XBy�Pu�|�v���Eb4T��`�><���A���l��i>�~T t&����-�O;B-�Q�����"%���N&�_�4��a~�w��9P����6�+��_��H��8�}
2��%~�()F���
���ښ����($�L�n-?������#��llED�:4���B��u���J+4x�D���8�<�
%j���)0� I� 1H%=�m����F��B��Q�'pÌ'�"�����6�-�i�{��K�t�~.r��D6�LxK��޹�p6h�x ̪n��*����-`��Xus��9l�%�DCRO���Ө	0"y���G��K�40�s�@�ѧa-%���>�\ɯ10Y�(�}�[�1��\n�]@�����LߕB�!Y�< '�N��_=�yFj����b5L�p��T��&�RI]ߙ����r���԰J,"�O�$�+�����,mS}�|*P+��_�� �.y��@��Pq�,�Ԃ�2�dK��O^�P�����r�SWͧ� T��Q_v`��,�Y��lU���dXm��D�h>f�a��?&�螇�
��E��.��ʧ�~���(��ȝ�K�Lق�{��������\6�x����o�ᆣY�r+D
_�u�8�R0J��^@��lI�b>l�j�ʘ�p&b�Y ���E'���D}Z���b�5 .7[�i���yW٢5O����2��q��/�i>�`��48��I
���=�R[,�R;�1�ƚ��s����\B1��P�4�����o1�s�G�E��4㡓���ԯ-�mP�+2����c�4��5�&*j��A��V�ePp��'2t��pv4{�dթ-#G�£�S�A֔�%1L����
����vnm��q�g���ll�l�[�.dr=}�����B
H+U$�`��i�[�,K3:�y�]�k�or���*n]������
 ϠD���A�e�?��':�_M=�4�yR�nj����#�
��MK�>��o/�7q^j`���(����ze|�tC��`�D�	�P�/0�9̊q��T�'�~�dj����Yb*�	��T�D�ʄ�H�MY���v��l�8�)˖��!�� #=�o�\����QҚ��*��7��^��0-�[�QX��-�����ol>��X��sZ?J��R��S��#�T;�ĭJ��*��q�@����zI��ҽ��1�i>�/ �z��A��ܑa�����%N#��4Oc�px� ����Q��ȴ#_;��~�7���ѓڏ�9����IE�{�yݎ�M�7r��ɤ��4$ �
1�	Ks?S!����FP���E�1'�㗬���S%��
�uE[Z�q+��b���J��6w�,�i����;*�����z����"��P^m3'���3n��XcS����z�^�0��o?�2�ʟ�p鵦n��߳F00��$HUp�Zjõ��k}&j��76�Z�S�V�d}k�eX�m�2,ί�Ī!��ʪ��&��[!/��>/36���YBt�a�N�#�[���);g!�7���NZ#���~�G|�~;Ji��&1�*#7D7��Yt��x��Z���j���Gq�fMF#Y�d�p��R��$K೰���IJ�\'u|�c�adoֱ�tQ�O���y	_������ӸE��?����|:���.=��{��hY��lY�
�Ei&s�b�6��Z-/���Y�?����2&v%�Kw��Q��e�Cb��wD�H*/6�ruq�T�={���#с׏�:�T&ͣ;6`��%f�!,eO��u��5P��/T	�yt�Q���-�@��|���w5δv$)�,OX8���D�R��/Ϫ�����%b��|\��ӟs�)Ĵ���]?�*>Xz�6ڌό&�����_�_ө/�3֎~�c�G���.��y	i�n��Y�yCح����"n��C�ZX}�z��m(FHKr�XU�e7$�2�^�v�H~	��6���{R�X�r� ����@�\��L�	q	�H�B9Jo����ך��=�M�h��~����O_>��o��g/z��G��YF�L,�OQ�8&��i�g��;Yn!���=��? �
:�`�9�K�Jw�v?��d٭���e��KO���:)�-��~�m�ȶ>5�hO��|��������u�����z�,����V4?5>�K�!���ZQx買�؋�PJ����~I�:�?��X"��Ë��-	�Y�/![�h0	�I�B�d7�݈	������{[E��C�c���n�Q%9��;=Pp���S�g�p��!�����Gm}u2S�-�$z���;? I���:���ƙ!�a�
oW�J�W�פ~�j5J�X4R�JM�&N�9�UA�MyZ��i>��5�n�mH܊�!]�K�U7s���˭V]6��%�2�����
�ߩ{�oޱK��(n�<1�o>�ؕ"����c��6��% Ӝ���~P�Q�:��f�3���J���y��ɰ8��Q�p|Hd��%,�7��̵y���wm�6��,���*�q�>}�������~�����P�ޏX��R���U9^�syifIW��4Y��0��D>�|�\�m��1$h��(O����4��p�!��.�?�}�	ByN�mF֏��Wz��g��7����U'��}1� ��ڻ���劁x��϶��Ȧ��8y�*Z!�-�n��@I����w��`_�C��r�(v*>�܁��fw�irӛ�W�؋;��65_��$xͲY��7����6�ѫB���<�+2e����~�-�.9a�^�l+�Nv�ѵIQT0�5�.�rF�/�t�$!-�gh�*,
�    E�k�=�h�����H�����y���~�Q">}!����4�|��策���7���a����}Pk��Dy&����5}p�ݽ�;(���d�H�Ġncb��������w=нha|d�$����w5�,�V��t��7q�n�
�
������%%)��g�\;]��BL��K
'R��`e�J]��0K���ʺԸ�iqa|�PT�j��GTOg��D��Q~dX��Ky��2,Ug���qΙ��[~0���.y�.�j,�f�>��?��.����@"�]��?��F����|OD��@y��c��w��{_&;H���w�~���W$"x�%Ϣp�%���}�����^��HtY.O�de>;/4&�ǝ�pL���'�^_}�,�#W9����[�}.`��j�#��^ٽ5ZV��8��J:tz��A�W9�TQ�`��������&=�<�"���� �� ���So�X��*]���e�'��t�/�&Γ���$]u��o�=q�
���h����IŸLŨ{��I1��b�Y��V�oۃom�i�
Z�~Y�qE�?�a�Ea�N�RȪ}Iy����Gu�	Qh��=�ȅ�|����[P���O���h���y �v�b�瀉�tu��1�,��(o���=A�^h+��T4�F����T������]�3�E��Np]�����
��XAj|u�-��y���o���=��ӟ�@AU��5����n����vy�ҟ���8���+�����ٜkws�=�#@R<R��]�C\�2��M�4 �Úq������ ��z��Q���dx���&�8e����8��1;��?M��Z����[�n_���rư�Ǔ�09����5���b?���O�_p���c�Az�A�#�D�H}ZK� ��\�Z��|!*�6~O�L(]��n4�IJ�����SJ�X��V����n�A@�����ܠ�'��A��$4t��%�����̀��"���[?�����3󧀶�v��8ثg�>�"؁����X��V�7���9]P?7�-y�|�IH+��Gu��ȋ�_�J��5��G����~Ϫ���+��<��O��E�G�m>6+ 8=�LF�`�����\���"��T�0I
=@�
]#������_O͔�t�m����n�y^��I�����*�MC\+�K8kP��vY�WɯB�]z�nf��nA�dY�E�'�ΒX���~G��m��e�x-ܚ| |0<�����h�W����n8�{M5���]Di�	��x��� P$�"!�~*9�D���3��x��x�y��ݿ�����}��ݿ�����~�*��n}Á;�Ҫ�b�i>��4��v� ���@Ы����m6ւ�ѪŠ`6*�O��9"Ab��Z>��>Cy�|T
@�_�C�3��0.Z)+��܎B��K|��M���� zz��Q;5�7P[Z�T�OPY8ɰ󊷬f��O��m�^	sɡ�����:�;�zLFK�b��X-�-���&��+_�u:�=���5�Ͷ@��=�T`�j�'��z�B�.7�6A�\��%R'�Ҵvaֳ<���.���T@J����7��[��'	f_隐ՔqR�r6�:$aN4E��t%�9?����D[3o+*��$�3z�Q��l%^M&e���)�
���DM��-i��H?;m���R�C9��eR*�F���[X#E����ua!z3�x�D΂HziO�6ה�渂k��aw8 �e���e���`=is9\鏞�V�ɳ��"�>^`"�B$:�Ţ>)����T���g�C(��H$gH��*ό��{{"Ͷ߳z�y󄉂�b9%� �����f��%�/x1}	R�pS�]_��VxPδ��� �-�o��Q+�����"���1V�Ӝ��<��卋�'��N�9��j��mԾ�VԷ��c�ô�"��lUvi�G��)i>S\�t��1E"�_t�.P3)�a�&li�9�L�k��K���E�-�n5�
joY��0{M���(E��hfW۲�'�z�wkZ�]�Y�4 ��$�	�S3���.���+�K_���#�F/-^��TB�e��r�2��)����π���pg���J���8���)�mj��@#�a�׃��D�odhf�Ek9�x���6������Z�% eB1�&�!�>��Y �X�@��	����o:�La`�@D�e$������
`$Cy�q> �������=�!�5f,b�����VJ����w��iw�<v(^�$�cp����XsNTSF^��G��B��9X��*���0���u�s��K�O�zI���	�;`���=�����;�$m�[�;�����Bׁ�Rp	?T�Vߒ�n�b�|aК�j<��*��@��	l��Z�MU���̀'�jL�b��T�B\(ا�guH�D�A�Gd�E C��f}�/߷�^d����^���0x}�~^#�Z��['/�ǐS�2^tSD�o��<9aB^+)�������Q*��@�7G�G�x%z�%�
��N����",���������EQ����)r�Qgr�b�[n>+��X!����rW�=���t�a���e���9��f<`S^�H[���,����F�ΙԌJL&
�.|�0'3�(�mM���>����oaHy
��0uT�W����,U���21_n>�\��Q8�S}��������\�ߙ����:a�"�S[�_�_@�S�T�xp�h�*bD�$6����s�R ��u}��G��Q��d�:��!@��!�JW�W�����Yg=�q$�,�&��`���;<=���0����
�S�\�.����f��'��,7��r�TS�Se� ��"$F��0чK�[�NlV��BWǵ(�|��qL��e+��O�*�ׯ��B�`�\f�������|��=�|TW�/1-)
BC1���)d�1�y��F�!j[��dy2�C�d�B�eS,�Z=�����ߪR�!���-T�(q%�����`0W�	X��ɹ�E��\�����M;�k��v��(�����@��s%Ig�5�WL&�x�;�hdn	G���$��4�^��+~���洔 j����e��K�S7����`W��H�än>B��-����hp��ժ.P3�e�H9W[P���=(fF
�g�Y���v��R�9��hb'�U�0�8U���
�#
gJ�')��a����gK�ˮ�}"�m5h�*�2B[j��F���_c-ן��yQY�ǈk>�pŚ���G�|�_�5ʛ�S�8��	��('�C/�D�#�oUX��6kkrسuF��a�X/m)�>"<����e�m=�B���A�C�nS�~���/E���|�+��T�Ϩ�zW���������N�cbd|�-(h$*�b"vKُ�j�뉎d=_����5�����jn�<˗�ʻdjldi3a[A�+$�"�\7@��8Z+���}����5�Z��g���qoz�2�o�������⿿���߿�&����7�������ݿ7�����c���;Z�+��[
<�;�*�.EMQY��F�l�YV7u�2闅�m�8nڷ�]_���5�+y�d�j�okb}��^�3��aglI6�L��.�(O�����8F�%��O�\�����-���ܡ��b0�;Zт�����w������s� @*�� U�}Ծ@������d08rz��~�p@����tb���a��Cg�jpA�%w3zwDM�eN����\k���0n�-0r�}����F�,@碤I �����6�����Ǭl��WR�T&���goI�o�\"���a����n��_?��_泆lY���d7FmǍ@��A{��v� �c���_3�@��Q�]�"��o��zvTF��TD]�����y	V��9u����&XG�m��`$0BW���4�=��	�h��G�9P0��.�[`��8��n��ӊ�p`-�i�d0��:=�Өl����ˊ+擓�Jw���Tu��[dX�A�� �  a���0,x�w�����x8�`�۽�b��(��y��}���-y�x��`Gf�p �+.R���Zo�1��ߡ�C�{�p��'�k�Nr2�?���E"��/֕�K�^X�џ��b	۠��D����j�����SS�MZ�Jȸ�æ+�J�y"����}P!�|�/aOU��%N��0�V�Y������9<��q�׳�@���ٴ�H��^�i��I#�ݼuK���2��R�1D����/}�����=~U��i��E�Mk5�)��>�`�K/ݚ� ��8Xk6��ߌfrJ4�����k�'��ߕ�٥=�����~����I��0���Gvn].���@�ilt|n����Fx%��Kk ������6��o���Ig��31�g����������������ED��1P�-��^�{���ڶ�6�i�v���D����(�n����
�Y ��f����^�����~J��5�xNhJ7��X{0��^q�,w�.�{M�r܆��$_�w�|]k���-4�g��S3�V_6�q%����.)/K@˫�K z�$�^�3����n�$�%��1�@t������?��Ù7�su�U��:���ɓ����c�U�' -Y�g�uk�x�*����G����K��]��q�|�{�0��l�W	_�sJ����c`q�"�)�{G=�m��)s���	�s��+o�����%����M��y�U�g;���� ��
\ΉO��GP [O�oX��p���k��-�7,lȻY��aJ�E���.k0��l��=�����٢�l�3C,�`~L~��`�}q�U�[�����9����Pk��������쪭�����:>��<���� H\6]�"y�fGDn��@%HJ��H1�|��^d�N%Uړ}xr�����J�8B/��pe*�j��/b�bl�o�>q��#���^Q0K��}����ż��P�f-2B������Ŧn            x������ � �         �  x���Ao� ���+�Gq�_�C�C.���^�]�����R�_�a[hܪ�%��Ӽo��@1Q�X�R��ly�Ҧ��Ă�`�2��@��LU\`�D<?h4=#s��u�7����d�c7������|@ MK���J1%�({��}?MC|�n< ��=z�2�є��iM�R\��b�˦� ix�Ьgm��޴�ʾ��u�gi��e��3��y炻��Ǎ�Y�e���Ԇm.H��7no��/'�:�-���O+By�䧹���z�Px\w��� ���(��]/\	PCQ���	rA�����ʰ���u�""� ��	������5���]��E9��
FF.�J��L.������¢�Z�-�\Pf�vvy���$K'|�'���Ԫ�b�e��5^#7�t������d�I�r)�9L.(���M&�]@ Ic�-�\�\�4���{!|����         �  x���KO�0��ί�4�Q]��w��(RiPH[��&@xH����h��8@MR!y�����s����	���S�Q+��I'�ςYr��򵘒�Es��_�oKH�Ԗ�q��`=K�uFiL~}���}����X8�ֳ(k��;�����Pn� 3
.��<N���|�)v�ژT�G�!:f(GP@F�2�8�"�~W�k�L��8��d���i|���M�D?դ R�A:.�H�%�E�͓r�)��_]@�����2+O�Aj ��'�,:��}����z؂=�ЎY5(��dT���tu�Iy(v���W �\T苔~�|TJk��!��*�0�Dg�2�7$�+����������;�zx��� �ȏ;�N�3Bs=t�j����O�U�����w�f���J���l�}�L4� �jÌW��۶x��C���������A���:            x������ � �            x������ � �            x������ � �         e  x��X�n7}����CѢ�"Y�/�c )�&/)�h��H˘Knx�������ʒ�Ҥ)�٫�p��̐��NF����|pr.F���h6�N������G���t_`:��O���q+���^��"|��yPA��Nxm�҈3[V����w=�6�}m �C���]�[h���B�9�+QJ�Tfl5c*7/�6[��*���,gbT݉S���m�L�����i_4����	��,��g�K���Z�^�I�y
�7�U�V��0�Z-�L��>�E���h8}ķ��V��efB��Կ���Z����)*g佲ޛ���\�U(DA�B|؜��ʣԾ/R��t6V�`rNo�
x/�d
	M"������jMYP+k�z0'���AHU��lWTd+�1�i[����Һ1ټ�s8j�}gЮ�p�mtu��v�g�.��pY�k*�P�%h/'{�T�5����@�����Y�o�*�#\�1+���b���/���Ҷ.��~R�ےP�L��������I��=~oz��X)Z?9��L={��ů�wղ�ie~�$����d��!�>Ď�N�k���Y����e_�������O�����A[�}���@��`;9���Sj��V��c��l���S>��î�ݿ=~~Y}������r���+|��{.qɫJ�Lf�����ԑYQ9e2���(��¨��a�5i���m� �II&��E[�y�ʈ �Ra�s?�9��x�,���묰��dU�ug2�tCޫ��m������˪�/���:
�V�=JnH�<�
IJ}FhZ*�e"<T@�h�&�M�^.��
��Ʈ5��$Q�li�>ٛS!�ww����
iK�5�fQ�蘁�	�+�:ܖA�@ɝ��Rk~����K���<��9�b�`S�}ԥw�۴�J6��[���� I�@�-e��n ���6��ڠV̆�$gKQHW2R�G�cUY��%l�]�
t�t���9�v_�	 �Mtk��,�ߎ� �bQG�R�Z(?�a�L;d�(O5ȕ�Ρ�t�M$1M��f陂-����������_�7��(gb|>��g������rt��3��M�f����9.1��7�\*�Ɗ�_��xJ%z�e��I����%�==n��w��A�7���/��x�6_��C��;7��@>��{k�K��Aw�N�Ҋ�FQ�u�dں��%�S2�Q~�%|h�>:J�RsR�����(�ܺf�%`�K����	}@fܔ+gM	�T6]�q�T *>D��Y;�y�t$�80#ܶ��b(�I��P��t�5ͱ��]=8��6���h�� ��G �deU�7�0L}p.�L ���-����]KYj�T����!��Y��3U��I�^�KhA)jL��#��jx܂"|Za� t	q��QI���/��x�򒋡� �3��.�*����Wc<=��� ����TO$��pYK�+l�I���)��PJ.c)�^(�U����L&�f��B�QeW�.;C�CP
����D��n�n8���s8����-Le�R�����˔���V�9�m�Ǻ�t�AV��,Ҡ���,>���a����Zv��{`쁱��~6Z���C�8t�C�����߳��u���.�Jq�vxtt�3���         6  x��X��7�uO1qa��J'˧�YR8�6�4P��.��CJ��I\�q��K�*i�W�G�p���6W�8� g���@N����l8���l>y0��G'gg�OϾO�'����Om�wۼ���m����m���o�˟{�ێ�����^��6������׶��[�>x��A�!Ha �@��^c6X֫%�L&����-i�usp����^�wwk�p#*��s��"X�xZ��~��C��΀�4$tj� �/�PhU�9��j�_�Z}<<����/k-�oao�vBk�"�yq�FT��r46�L�FA�!�C턢�U���:ӱ�N��Bm�3 @ڊ{>)%k�Q�/hO��;�^1+��B� �#�dt�/������_fݲ���A�(Cř.E]+�D#xb!V��9JO䊴�@�n�5Ջ�q,�j�v��q�L�(�����<���eV���*��H�'ԛ�(E��u�f:g���������z�0�٘]Nt�R�W�p�����	�+�!|����8T��\N��dL�&��Z�ī(�]M�ݸ`��n�<��F�e��\��ѯ}<�Ս���~xVJA��r2T�*���
b�J�lC�G��8�jge��T���P�C6F����v�*ީ-�4G-b�d�t��� ��%ntQ�@n:e�S�{R�;�N*���@���S�z}H�O�Oп���|H�'��<~���j�p���������t<�������C���b�T`����U�e���ٙf��<;?zB����щߥ����t�J�O�O�Oп!>����?�)%>�<���         c  x��Mo�0���W�m۲	!P�!6R�(]UtO��d .�m�,�~'�+A�ݪ���-V���+?R��'��xx�@���Ѹ?�f�� |��q?�8 ��8�,g��(/��9�d4Wk�i���-X^kq����8��@��U�p�� �%��F�Ο�,�̡��*fa�(}/T�Q>3�iܡ�Ʋ@�dql�`_Q6��ȭT{y@�N�-0�0!��Ց����+�%pGaP�-�8��^S��wa�Bۘ�Q�T4����t��kGR�j0a�W&�i�J���%yq��+�0.۵��T����PTLn����X+�f�m������=w�?j�,���zt`+Dg�Pҽt9]�fֲM+�]FMo�H;
)�eK�[�4�(I�(��Q�43��[�}֛h�t�
X+��j��f%���|��nO�iWA�	~P�;Zݓy���wG���G7�m�m>�/o�[d�U�*�@�yT��Q��
���d@Upǻ�J_�h���䋟����]�XF��q�}.����Vz�4@+�#@+��A���I���5�^:��qr�M��`4z	�>Η�y4m�4\F�h7��9�����K>�K0������N�7��z      !   �   x��ν�0@�}���a�D�\]��Bޚ������0���|�H.ʄ�̙P����R�
���WY>�1���<�m]��uH���c`��6-��N��5[�P��uD��YmB�ayD�Ez�7$R9��rF�%�S'�:9��x���M�ߺC�?55��pJ)�//~�      #      x������ � �      %   �   x�M�Yn�0D���U�6w)P�2!+�DA�\��U���q\Мb�Ug�w< 5[�pMQ5J�㓄*���)�r�3x��cv/��/��H�(�|R*h�fM�s����P�s���{%���'��a�R��ި2�;�k�Y�Awڝ��yHٸn���2P��{���+�߸}e����0�(�֖�d 3�K���!�/��w�      '   �  x����r�8���S��V\���Y�J倖x�ƌm2��o;kA�e��[�ӧ���p��i�?�q�)� ��vn�n�
���Ϩ ����U�8c�R�f����2�>Z�+f�V��h������\�T�����C7���#���b���E[T�W��b芑�����p
�t�1�_�v/Rq�(�g�H����AX��S��"M+�f7114��p�c�N�R؂}��x:�[E�5���F��v��'�M�=����~qZ~>��sY*���n���0:�t�4/�he����m4��9�2"��K;nj�!,~��a�ֵk=oM��y�OG�q���ij�ߘA;�7�tE�hn�vn���_����L2�D�Ҏ�p9�	�K�fA��E������9���et�y����ԜϏ" +6!M/�l�7��"e���*�κdf��Ұ��tyi�����2Ġ�6��� |_�/#[�'d}��Ay1S	2�U���! T�
�
� e�Đ�����a/������2we]X!@r-r�s�qA�rj1�� ��)֤�g��"�g#݁oy�cfPqz���J�B/�a�"�*�x�4��j+S�����9i@�K�s|41T�-%9I��N��k��a��s��ݎ'���X��UdN�	ɵ�Iݘ��ї��J�̪�_\@P��|M�̝b���d~�^�ߟ*�A���
c+K�c2e6W���eJ��u����)�ܟ�f+F̥��%=k�ʱ��>��ti+��?�~n�؄��7Sk�Ǔ���r�������ck�5�rt*���T{�I��&#�8��g��p��������.�њ��B�>�o]n�͔�r�U�H6'F�U����I��W���h0ٝ3�"o����������jP9Ē�WOOO�i�
L      )   �  x���Q��0 ���W�~9�
��.�j0F�
4�"���
(Zʯ_5�ݟ0�y�df�dF�=vsȚ}7fM}�T��=�W��F#�
�y��rLyX��/��m�\E��T�(U��Wa�F4�)o_��H6MbnG�,�x�V�|�X��XT�"	E.K��Dc\��at�V�,��2�fǍ���%M�`������ڈ�cY���\�����Q7'0G[����Qy�i���=���Le ��|΂Q�`Vj:�x��T o�p_�G��H~�Shz�x�2���? �,<q.
�mŋq����J�7;p�%�y�=��޸�lþ���/__���q6��A��>6#��]`^�y���A0��gb[��,>�rŉ������k(�{i���E��x���n�k���y�A|Ѹ7O&wu-��]Wr+v��N�п�|p�������8.���ǘL&��[N      *   n   x�E�A�0�5=�{3嗙�N��K!!��M�x��B���g��{i�uܗ�>u�ʹWi;#���1B�#	�'Y����`�`��o��,)'��jP�C��{x��nK�     