from django.urls import path
from .views import *
app_name = 'cmsapp'
_url_admin = 'cms-admin/'
urlpatterns = [


    path(_url_admin + "message/list/",
         AdminMessageListView.as_view(), name='adminmessagelist'),
    path(_url_admin + "<int:pk>/detail/",
         AdminMessageDetail.as_view(), name='adminmessagedetail'),


    path(_url_admin, AdminHomeView.as_view(), name='adminhome'),


    path('login/', LoginView.as_view(), name="login"),

    path('logout/', LogoutView.as_view(), name="logout"),

    path(_url_admin + "notice/list/",
         AdminNoticeListView.as_view(), name='adminnoticelist'),

    path(_url_admin + "notice/create/",
         AdminNoticeCreateView.as_view(), name='adminnoticecreate'),

    path(_url_admin + "notice/<int:pk>/delete/",
         AdminNoticeDeleteView.as_view(), name='adminnoticedelete'),

    path(_url_admin + "notice/<int:pk>/update/",
         AdminNoticeUpdateView.as_view(), name='adminnoticeupdate'),

    path(_url_admin + "event/list/",
         AdminEventListView.as_view(), name="admineventlist"),
    path(_url_admin + "event/create/",
         AdminEventCreateView.as_view(), name="admineventcreate"),
    path(_url_admin + "event/<int:pk>/update/",
         AdminEventUpdateView.as_view(), name="admineventupdate"),
    path(_url_admin + "event/<int:pk>/delete/",
         AdminEventDeleteView.as_view(), name="admineventdelete"),

    path(_url_admin + "blog/list/",
         AdminBlogListView.as_view(), name="adminbloglist"),
    path(_url_admin + "blog/create/",
         AdminBlogCreateView.as_view(), name="adminblogcreate"),
    path(_url_admin + "blog/<int:pk>/update/",
         AdminBlogUpdateView.as_view(), name="adminblogupdate"),
    path(_url_admin + "blog/<int:pk>/delete/",
         AdminBlogDeleteView.as_view(), name="adminblogdelete"),



    path(_url_admin + "gallery/list/",
         AdminGalleryListView.as_view(), name='admingallerylist'),
    path(_url_admin + "gallery/create/",
         AdminGalleryCreateView.as_view(), name='admingallerycreate'),
    path(_url_admin + "gallery/<int:pk>/update/",
         AdminGalleryUpdateView.as_view(), name='admingalleryupdate'),
    path(_url_admin + "gallery/<int:pk>/delete/",
         AdminGalleryDeleteView.as_view(), name='admingallerydelete'),

    path(_url_admin + "logo/list/",
         AdminLogoListView.as_view(), name='adminlogolist'),
    path(_url_admin + "logo/create/",
         AdminLogoCreateView.as_view(), name='adminlogocreate'),
    path(_url_admin + "logo/<int:pk>/update/",
         AdminLogoUpdateView.as_view(), name='adminlogoupdate'),
    path(_url_admin + "logo/<int:pk>/delete/",
         AdminLogoDeleteView.as_view(), name='adminlogodelete'),

    path(_url_admin + "aboutus/list/",
         AdminAboutusListView.as_view(), name='adminaboutuslist'),
    path(_url_admin + "aboutus/create/",
         AdminAboutusCreateView.as_view(), name='adminaboutuscreate'),
    path(_url_admin + "aboutus/<int:pk>/update/",
         AdminAboutusUpdateView.as_view(), name='adminaboutusupdate'),

    path(_url_admin + "newsevent/list/", AdminNewseventListView.as_view(), name='adminnewseventlist'),
    path(_url_admin + "newsevent/<int:pk>/detail/", AdminNewseventDetailView.as_view(), name='adminnewseventdetail'),    
    path(_url_admin + "newsevent/create/", AdminNewseventCreateView.as_view(), name='adminnewseventcreate'),
    path(_url_admin + "newsevent/<int:pk>/update/", AdminNewseventUpdateView.as_view(), name='adminnewseventupdate'),
    path(_url_admin + "newsevent/<int:pk>/delete/", AdminNewseventDeleteView.as_view(), name='adminnewseventdelete'),


    path(_url_admin + "projectarea/list/", AdminProjectAreaListView.as_view(), name='adminprojectarealist'),
    path(_url_admin + "projectarea/<int:pk>/detail/", AdminProjectAreaDetailView.as_view(), name='adminprojectareadetail'),    
    path(_url_admin + "projectarea/create/", AdminProjectAreaCreateView.as_view(), name='adminprojectareacreate'),
    path(_url_admin + "projectarea/<int:pk>/update/", AdminProjectAreaUpdateView.as_view(), name='adminprojectareaupdate'),
    path(_url_admin + "projectarea/<int:pk>/delete/", AdminProjectAreaDeleteView.as_view(), name='adminprojectareadelete'),


    path(_url_admin + "menubar/list/", AdminMenubarListView.as_view(), name='adminmenubarlist'),
    path(_url_admin + "menubar/<int:pk>/detail/", AdminMenubarDetailView.as_view(), name='adminmenubardetail'),    
    path(_url_admin + "menubar/create/", AdminMenubarCreateView.as_view(), name='adminmenubarcreate'),
    path(_url_admin + "menubar/<int:pk>/update/", AdminMenubarUpdateView.as_view(), name='adminmenubarupdate'),
    path(_url_admin + "menubar/<int:pk>/delete/", AdminMenubarDeleteView.as_view(), name='adminmenubardelete'),


    path(_url_admin + "slideshow/list/", AdminSlideShowListView.as_view(), name='adminslideshowlist'),
    path(_url_admin + "slideshow/create/", AdminSlideShowCreateView.as_view(), name='adminslideshowcreate'),
    path(_url_admin + "slideshow/<int:pk>/update/", AdminSlideShowUpdateView.as_view(), name='adminslideshowupdate'),
    path(_url_admin + "slideshow/<int:pk>/delete/", AdminSlideShowDeleteView.as_view(), name='adminslideshowdelete'),

    path(_url_admin + "overview/create/", AdminOverviewCreateView.as_view(), name="adminoverviewcreate"),
    path(_url_admin + "overview/list/", AdminOverviewListView.as_view(), name="adminoverviewlist",),
    path(_url_admin + "overview/<int:pk>/delete/", AdminOverviewDeleteView.as_view(), name="adminoverviewdelete"),
    path(_url_admin + "overview/<int:pk>/update/", AdminOverviewUpdateView.as_view(), name = "adminoverviewupdate"),

    path(_url_admin + "subscriber/list/", AdminSubscriberListView.as_view(), name = "adminsubscriberlist"),
    path(_url_admin + "subscriber/<int:pk>/delete/", AdminSubscriberDeleteView.as_view(), name = "adminsubscriberdelete"),
    #path(_url_admin + "subscriber/<int:pk>/detail/", AdminSubscriberDetailView.as_view(), name = "adminsubscriberdetail"),

    path(_url_admin + "personcat/create/", AdminPersonCatCreateView.as_view(), name="adminpersoncatcreate"),
    path(_url_admin + "personcat/list/", AdminPersonCatListView.as_view(), name="adminpersoncatlist",),
    path(_url_admin + "personcat/<int:pk>/delete/", AdminPersonCatDeleteView.as_view(), name="adminpersoncatdelete"),
    path(_url_admin + "personcat/<int:pk>/update/", AdminPersonCatUpdateView.as_view(), name = "adminpersoncatupdate"),

    path(_url_admin + "person/create/", AdminPersonCreateView.as_view(), name="adminpersoncreate"),
    path(_url_admin + "person/list/", AdminPersonListView.as_view(), name="adminpersonlist",),
    path(_url_admin + "person/<int:pk>/delete/", AdminPersonDeleteView.as_view(), name="adminpersondelete"),
    path(_url_admin + "person/<int:pk>/update/", AdminPersonUpdateView.as_view(), name = "adminpersonupdate"),

    path(_url_admin + "interested/create/", AdminInterestedCreateView.as_view(), name="admininterestedcreate"),
    path(_url_admin + "interested/list/", AdminInterestedListView.as_view(), name="admininterestedlist",),
    path(_url_admin + "interested/<int:pk>/delete/", AdminInterestedDeleteView.as_view(), name="admininteresteddelete"),
    path(_url_admin + "interested/<int:pk>/update/", AdminInterestedUpdateView.as_view(), name = "admininterestedupdate"),


    path(_url_admin + "sitesetting/create/", AdminSiteSettingCreateView.as_view(), name="adminsitesettingcreate"),
    path(_url_admin + "sitesetting/list/", AdminSiteSettingListView.as_view(), name="adminsitesettinglist",),
    path(_url_admin + "sitesetting/<int:pk>/delete/", AdminSiteSettingDeleteView.as_view(), name="adminsitesettingdelete"),
    path(_url_admin + "sitesetting/<int:pk>/update/", AdminSiteSettingUpdateView.as_view(), name = "adminsitesettingupdate"),
    path(_url_admin + "sitesetting/<int:pk>/updateU/", AdminSiteSettingUpdateUView.as_view(), name = "adminsitesettingupdateU"),

    path(_url_admin + "testimonial/create/", AdminTestimonialCreateView.as_view(), name="admintestimonialcreate"),
    path(_url_admin + "testimonial/list/", AdminTestimonialListView.as_view(), name="admintestimoniallist",),
    path(_url_admin + "testimonial/<int:pk>/delete/", AdminTestimonialDeleteView.as_view(), name="admintestimonialdelete"),
    path(_url_admin + "testimonial/<int:pk>/update/", AdminTestimonialUpdateView.as_view(), name = "admintestimonialupdate"),

    path(_url_admin + "reportcat/create/", AdminReportCatCreateView.as_view(), name="adminreportcatcreate"),
    path(_url_admin + "reportcat/list/", AdminReportCatListView.as_view(), name="adminreportcatlist",),
    path(_url_admin + "reportcat/<int:pk>/delete/", AdminReportCatDeleteView.as_view(), name="adminreportcatdelete"),
    path(_url_admin + "reportcat/<int:pk>/update/", AdminReportCatUpdateView.as_view(), name = "adminreportcatupdate"),

    path(_url_admin + "report/create/", AdminReportCreateView.as_view(), name="adminreportcreate"),
    path(_url_admin + "report/list/", AdminReportListView.as_view(), name="adminreportlist",),
    path(_url_admin + "report/<int:pk>/delete/", AdminReportDeleteView.as_view(), name="adminreportdelete"),
    path(_url_admin + "report/<int:pk>/update/", AdminReportUpdateView.as_view(), name = "adminreportupdate"),

    path(_url_admin + "settings/", AdminSettingsView.as_view(), name="adminsettingslist",),
    path(_url_admin + "idchecker/",IdCheckerView.as_view(), name="idchecker"),

    path(_url_admin + "registration/", AdminUserInfoRegView.as_view(), name="adminuserinfocreate"),
    path(_url_admin + "userinfo/<int:pk>/update/", AdminUserInfoUpdateView.as_view(), name="adminuserinfoupdate"),
    path(_url_admin + "userinfo/list/", AdminUserInfoListView.as_view(), name="adminuserinfolist"),
    path(_url_admin + "userinfo/<int:pk>/delete/", AdminUserInfoDeleteView.as_view(), name="adminuserinfodelete"),
    path(_url_admin + "userinfo/<int:pk>/detail/", AdminUserInfoDetailView.as_view(), name="adminuserinfodetail"),
    




    ##################################################################
    path('', ClientHomeView.as_view(), name='clienthome'),

    path('home/', ClientHomeView.as_view(), name='clienthome'),

    path("gallery/", ClientGalleryListView.as_view(), name="clientgallerylist"),

    path("blog/", BlogListView.as_view(), name="bloglist"),
    #path("blog/<int:pk>/detail/", BlogDetailView.as_view(), name="blogdetail"),
    path("blog/<slug:slug>/detail/", BlogDetailView.as_view(), name="blogdetail"),

    path("overview/<int:pk>/detail/", OverviewDetailView.as_view(), name="overviewdetail"),

    path("ourteam/",PersonListView.as_view(), name="outeam"),

    path("aboutus/<int:pk>/detail/", AboutDetailView.as_view(), name='aboutusdetail'),

    path("newsevents/",NewsEventListView.as_view(), name='newseventlist'),
    path("newsevents/<int:pk>/detail/", NewsEventDetailView.as_view(), name='newseventdetail'),

    path("contact/", MessageCreateView.as_view(), name='messagesender'),
    path("projectarea/<int:pk>/detail/", ProjectAreaDetailView.as_view(), name='projectareadetail'),

    path("report/", ReportListView.as_view(), name='repostlist'),

    
]
