from django.db import models
from django.contrib.auth.models import User, Group



class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class SiteSetting(TimeStamp):
    name = models.CharField(max_length=200)
    value = models.CharField(max_length=600, blank=True, null=True)
    sitegroup = models.CharField(max_length=50)
    published = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Userinfo(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200, blank=True, null=True)
    company_name = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to="users")
    phone = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    email = models.EmailField()
    website = models.CharField(max_length=200, blank=True, null=True)
    about = models.TextField()

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name="admin")
        self.user.groups.add(group)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.full_name

class MenuCat(TimeStamp):
    title = models.CharField(max_length=100)
    text = models.TextField(blank=True, null=True)
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Menubar(TimeStamp):
    title = models.CharField(max_length=150)
    root = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    menu_type = models.ForeignKey(MenuCat, on_delete=models.CASCADE, blank=True, null=True)
    slug = models.CharField(max_length=50, blank=True, null=True)
    link = models.CharField(max_length=100, blank=True, null=True)
    icon = models.CharField(max_length=20, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Message(TimeStamp):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    email = models.EmailField(null=True, blank=True)
    subject = models.CharField(max_length=220)
    text = models.TextField()

    def __str__(self):
        return self.name

class Subscriber(TimeStamp):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    subject = models.CharField(max_length=420)
    phone = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class Testimonial(TimeStamp):
    name = models.CharField(max_length=100)
    email = models.EmailField(blank=True, null=True)
    address = models.CharField(max_length=420,blank=True, null=True)
    workat = models.CharField(max_length=140, blank=True, null=True)
    text = models.TextField()
    image = models.ImageField(upload_to="testimonial", blank=True, null=True)

    def __str__(self):
        return self.name


class Notice(TimeStamp):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Event(TimeStamp):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='events')
    content = models.TextField()
    date_of_event = models.DateField()
    venue = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Blog(TimeStamp):
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=150, unique=False, allow_unicode=True)
    subtle = models.TextField()
    text = models.TextField()
    image = models.ImageField(upload_to='blogs')
    author = models.CharField(max_length=90)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

   
    def __str__(self):
        return self.title


class Logo(TimeStamp):
    title = models.CharField(max_length=100)
    alt = models.CharField(max_length=200)
    image = models.ImageField(upload_to='logos')


    def __str__(self):
        return self.title

class Gallery(TimeStamp):
    title = models.CharField(max_length=100)
    caption = models.CharField(max_length=200)
    menubar = models.ForeignKey(Menubar, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='galleries')
    published = models.BooleanField(default=True)
    priority = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.title


class ReportCat(TimeStamp):
    title = models.CharField(max_length=100)
    priority = models.IntegerField(blank=True, null=True)


    def __str__(self):
        return self.title


class Report(TimeStamp):
    title = models.CharField(max_length=100)
    r_type = models.ForeignKey(ReportCat, on_delete=models.CASCADE)
    caption = models.CharField(max_length=200)
    image = models.ImageField(upload_to='reports', blank=True, null=True)
    file = models.FileField(upload_to='reportfiles')
    priority = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.title


class Aboutus(TimeStamp):
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=200)
    subtle = models.TextField()
    slug = models.CharField(max_length=200, blank=True, null=True)
    image = models.ImageField(upload_to='aboutus')
    caption = models.CharField(max_length=100)
    text = models.TextField()

    def __str__(self):
        return self.title


class Overview(TimeStamp):
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=200, blank=True, null=True)
    menubar = models.OneToOneField(Menubar, on_delete=models.CASCADE)
    text = models.TextField()
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class Newsevent(TimeStamp):
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=200, blank=True, null=True)
    category = models.ForeignKey(Menubar, on_delete=models.CASCADE)
    subtle = models.TextField(blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to='newsevents', blank=True, null=True)
    caption = models.CharField(max_length=150, blank=True, null=True)
    text = models.TextField()
    published = models.BooleanField(default=True)
    thumbnail_limit = models.IntegerField()

    def __str__(self):
        return self.title


class ProjectArea(TimeStamp):
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=200, blank=True, null=True)
    subtle = models.TextField(max_length=500, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to='projectareas')
    caption = models.CharField(max_length=150, blank=True, null=True)
    text = models.TextField()

    def __str__(self):
        return self.title


class PersonCat(TimeStamp):
    title = models.CharField(max_length=100)
    priority = models.IntegerField(blank=True, null=True)


    def __str__(self):
        return self.title


class Person(TimeStamp):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(PersonCat, on_delete=models.CASCADE)
    post = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=150, blank=True, null=True)
    image = models.ImageField(upload_to='person')
    text = models.TextField()
    nationality = models.CharField(max_length=150, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name

class Interested(TimeStamp):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=150, blank=True, null=True)
    image = models.ImageField(upload_to='interested', blank=True, null=True)
    amount = models.IntegerField(blank=True, null=True)
    country = models.CharField(max_length=150, blank=True, null=True)
    text = models.TextField()
    read = models.BooleanField(default=False)
    readby= models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name


class SlideShow(TimeStamp):
    title = models.CharField(max_length=90)
    caption = models.CharField(max_length=90, blank=True, null=True)
    priority = models.IntegerField(blank=True, null=True)
    image = models.ImageField(upload_to='slideshows')
    published = models.BooleanField(default=0)

    def __str__(self):
        return self.title
