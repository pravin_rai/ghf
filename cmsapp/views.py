from django.core.paginator import Paginator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.http import JsonResponse
from datetime import date

_url_admin = '/cms-admin/'

class BaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['events'] = Event.objects.all().order_by('-id')
        context['newsevents'] = Newsevent.objects.all().order_by('-id')
        context['projectareas'] = ProjectArea.objects.all().order_by('priority')
        context['home_aboutus'] = Aboutus.objects.all()
        context['header_aboutus'] = Aboutus.objects.all().order_by('-id')
        context['header_testimonials'] = Testimonial.objects.all().order_by('-id')[:2]
        context['header_blogs'] = Blog.objects.all().order_by('-id')[:3]
        mainMenubars =  Menubar.objects.filter(published=True,
            root=None).order_by('priority')[:7]
        context['mainMenubars'] = mainMenubars
        context['overviews'] = Overview.objects.all()
        context['slideshows'] = SlideShow.objects.filter(published=True)[:8]
        context['homegalleries'] = Gallery.objects.filter(published=True).order_by('-id')[:6]
        context['sitesettings'] = SiteSetting.objects.filter(published=True)
        context['logosets'] = Logo.objects.all()


        return context

class AdminBaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usr = self.request.user
        context['base_userinfo'] = Userinfo.objects.get(user=usr)


        return context


class MenubarMixin(object):
    def get_context_data(self, **kwargs):
        context['mainMenubars'] = Menubar.objects.filter(published='0')[:6]

        return context

class AdminSettingsView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = "/login/"    
    template_name = 'admintemplates/adminsettingslist.html'
    model = SiteSetting
    context_object_name = 'sitesettings'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['logos'] = Logo.objects.all().order_by('-id')
        context['reportcats'] = ReportCat.objects.all()
        context['personcats'] = PersonCat.objects.all()


        

        return context

class AdminUserInfoRegView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = "/login/"    
    template_name = "admintemplates/adminuserinfocreate.html"
    form_class = UserinfoForm
    success_url =  _url_admin + 'userinfo/list/'

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        user = User.objects.create_user(uname, " ", pword)
        form.instance.user = user
        login(self.request, user)

        return super().form_valid(form)

class AdminUserInfoUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = "/login/"    
    template_name = "admintemplates/adminuserinfocreate..html"
    form_class = UserinfoForm
    model = Userinfo
    success_url = _url_admin + 'userinfo/list/'

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        user = User.objects.create_user(uname, " ", pword)
        form.instance.user = user
        login(self.request, user)

        return super().form_valid(form)


class AdminUserInfoListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = "/login/"
    template_name = "admintemplates/adminuserinfolist.html"
    queryset = Userinfo.objects.all();
    context_object_name = "userinfolists"



class AdminUserInfoDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = "/login/"
    template_name = "admintemplates/adminuserinfodelete.html"
    model = Userinfo
    success_url =  _url_admin + 'userinfo/list/'

    

class AdminUserInfoDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = "/login/"
    template_name = "admintemplates/adminuserinfodetail.html"
    model = Userinfo
    context_object_name = 'userinfo'


class IdCheckerView(View):
    def get(self, request):
        uname = request.GET.get("user_name")
        #name = User.objects.filter(username=uname).exists()
        if User.objects.filter(username=uname).exists():
            message = "Not Available"
            color = "red"
        else:
            message = "Available"
            color = "green"
        return JsonResponse({
            "message": message,
            "color": color,
        })

#Dashboard
class AdminHomeView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = "/login/"
    template_name = 'admintemplates/adminhome.html'
    queryset = Newsevent.objects.all().count()
    context_object_name = 'eventnum'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['blognum'] = Blog.objects.all().count()
        context['gallerynum'] = Gallery.objects.all().count()
        context['teamnum'] = Person.objects.all().count()
        context['testimonialnum'] = Testimonial.objects.all().count()
        context['reportnum'] = Report.objects.all().count()
        context['slideshownum'] = SlideShow.objects.all().count()
        context['menunum'] = Menubar.objects.all().count()
        context['sitesettingnum'] = SiteSetting.objects.all().count()
        context['userinfonum'] = Userinfo.objects.all().count()



        return context


class AdminMessageListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = "/login/"

    template_name = 'admintemplates/adminmessagelist.html'
    model = Message
    context_object_name = 'messagelist'


class AdminMessageDetail(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = "/login/"

    template_name = 'admintemplates/adminmessagedetail.html'
    model = Message
    context_object_name = 'messageobject'





class LoginView(FormView):
    template_name = "admintemplates/login.html"
    form_class = LoginForm
    success_url = "/cms-admin/"

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]

        user = authenticate(username=uname, password=pword)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, self.template_name,
                          {'form': form,
                           'error': 'You have entered wrong username or password'})

        return super().form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/")


class AdminNoticeListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnoticelist.html'
    queryset = Notice.objects.all().order_by('-id')
    context_object_name = 'noticelist'


class AdminNoticeCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnoticecreate.html'
    form_class = NoticeForm
    success_url = _url_admin + 'notice/list/'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminNoticeUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnoticecreate.html'
    model = Notice
    form_class = NoticeForm
    success_url = _url_admin + 'notice/list/'


class AdminNoticeDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    template_name = 'admintemplates/adminnoticedelete.html'
    model = Notice
    success_url = _url_admin + 'notice/list/'


class AdminEventListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/admineventlist.html'
    queryset = Event.objects.all().order_by('-id')
    context_object_name = 'eventlist'


class AdminEventCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/admineventcreate.html'
    model = Event
    form_class = EventForm
    success_url = _url_admin + 'event/list'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminEventUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/admineventcreate.html'
    model = Event
    form_class = EventForm
    success_url = _url_admin + 'event/list'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminEventDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    template_name = 'admintemplates/admineventdelete.html'
    model = Event
    success_url = _url_admin + 'event/list/'


class AdminBlogListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminbloglist.html'
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = 'blogs'


class AdminBlogCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = "/login/"
    template_name = 'admintemplates/adminblogcreate.html'
    form_class = BlogForm
    success_url = _url_admin + 'blog/list/'

    def form_valid(self, form):
        writer = self.request.user
        form.instance.created_by = writer
        slug_title = form.cleaned_data['title']
        slug_title = slug_title.replace(',', '')            
        words = slug_title.split()
        dashed =""
        today = date.today()
        for word in words:
            dashed += word + "-"

        slug = dashed + today.strftime("%m-%d-%Y")
        form.instance.slug = slug

        return super().form_valid(form)



        # def form_valid(self, form):
        # form.instance.admin = self.request.user
        # teacher = form.cleaned_data['name']
        # scale = form.cleaned_data['scale']
        # form.instance.i_tax = 0.01 * scale
        # form.instance.p_fund = 0.15 * scale
        # form.instance.ss_fund = 0.12 * scale
        # form.instance.last_sal = scale - \
        #     (0.01 * scale + 0.15 * scale + 0.12 * scale)
        # teacher.t_sal += scale - \
        #     (0.01 * scale + 0.15 * scale + 0.12 * scale)
        # teacher.save()
        # form.instance.leaveday_total = 5

        # return super().form_valid(form)


class AdminBlogUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admintemplates/adminblogcreate.html'
    form_class = BlogForm
    model = Blog
    success_url = _url_admin + 'blog/list/'


class AdminBlogDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = "/login/"
    template_name = 'admintemplates/adminblogdelete.html'
    model = Blog
    success_url = _url_admin + 'blog/list/'

    #Logos list
class AdminLogoListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminlogolist.html'
    queryset = Logo.objects.all()
    context_object_name = 'logos'

class AdminLogoCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminlogocreate.html'
    form_class = LogoForm
    success_url = _url_admin + 'settings/'

class AdminLogoUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminlogocreate.html'
    form_class = LogoForm
    model = Logo
    success_url = _url_admin + 'settings/'

class AdminLogoDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminlogodelete.html'
    model = Logo
    success_url = _url_admin + 'settings/'

class AdminGalleryListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/admingallerylist.html'
    queryset = Gallery.objects.all().order_by('-id')
    context_object_name = 'galleries'


class AdminGalleryCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/admingallerycreate.html'
    form_class = GalleryForm
    success_url = _url_admin + 'gallery/list/'


class AdminGalleryUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/admingallerycreate.html'
    form_class = GalleryForm
    model = Gallery
    success_url = _url_admin + 'gallery/list/'


class AdminGalleryDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/admingallerydelete.html'
    model = Gallery
    success_url = _url_admin + 'gallery/list/'

class AdminReportListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportlist.html'
    queryset = Report.objects.all().order_by('-id')
    context_object_name = 'reports'


class AdminReportCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportcreate.html'
    form_class = ReportForm
    success_url = _url_admin + 'report/list/'


class AdminReportUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportcreate.html'
    form_class = ReportForm
    model = Report
    success_url = _url_admin + 'report/list/'


class AdminReportDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportdelete.html'
    model = Report
    success_url = _url_admin + 'report/list/'
    
class AdminAboutusListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = "/login/"
    template_name = 'admintemplates/adminaboutuslist.html'
    queryset = Aboutus.objects.all().order_by('-id')
    context_object_name = 'aboutuss'


class AdminAboutusCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = "/login/"
    template_name = 'admintemplates/adminaboutuscreate.html'
    form_class = AboutusForm
    success_url = _url_admin + 'aboutus/list/'


class AdminAboutusUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admintemplates/adminaboutuscreate.html'
    form_class = AboutusForm
    model = Aboutus
    success_url = _url_admin + 'aboutus/list/'


class AdminNewseventListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnewseventlist.html'
    queryset = Newsevent.objects.all().order_by('priority')
    context_object_name = 'newsevents'


class AdminNewseventCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnewseventcreate.html'
    model = Newsevent
    form_class = NewseventForm
    success_url = _url_admin + 'newsevent/list/'


class AdminNewseventUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnewseventcreate.html'
    form_class = NewseventForm
    model = Newsevent
    success_url = _url_admin + 'newsevent/list/'


class AdminNewseventDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnewseventdelete.html'
    model = Newsevent
    success_url = _url_admin + 'newsevent/list/'


class AdminNewseventDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = '/login/'
    template_name = 'admintemplates/adminnewseventdetail.html'
    queryset = Newsevent.objects.all().order_by('-id')
    context_object_name = 'projetarea'


class AdminProjectAreaListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectarealist.html'
    queryset = ProjectArea.objects.all().order_by('priority')
    context_object_name = 'projetareas'


class AdminProjectAreaCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectareacreate.html'
    model = ProjectArea
    form_class = ProjectAreaForm
    success_url = _url_admin + 'projectarea/list/'


class AdminProjectAreaUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectareacreate.html'
    form_class = ProjectAreaForm
    model = ProjectArea
    success_url = _url_admin + 'projectarea/list/'


class AdminProjectAreaDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectareadelete.html'
    model = ProjectArea
    success_url = _url_admin + 'projectarea/list/'


class AdminProjectAreaDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectareadetail.html'
    queryset = ProjectArea.objects.all().order_by('-id')
    context_object_name = 'projetarea'


class AdminPersonListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersonlist.html'
    queryset = Person.objects.all().order_by('priority')
    context_object_name = 'persons'


class AdminPersonCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncreate.html'
    model = Person
    form_class = PersonForm
    success_url = _url_admin + 'person/list/'


class AdminPersonUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncreate.html'
    form_class = PersonForm
    model = Person
    success_url = _url_admin + 'person/list/'


class AdminPersonDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersondelete.html'
    model = Person
    success_url = _url_admin + 'person/list/'


class AdminPersonDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersondetail.html'
    queryset = Person.objects.all().order_by('-id')
    context_object_name = 'person'

class AdminMenubarListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminmenubarlist.html'
    queryset = Menubar.objects.all()
    context_object_name = 'menubars'


class AdminMenubarCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminmenubarcreate.html'
    form_class = MenubarForm
    success_url = _url_admin + 'menubar/list/'


class AdminMenubarUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminmenubarcreate.html'
    form_class = MenubarForm
    model = Menubar
    success_url = _url_admin + 'menubar/list/'


class AdminMenubarDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminmenubardelete.html'
    model = Menubar
    success_url = _url_admin + 'menubar/list/'


class AdminMenubarDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = '/login/'
    template_name = 'admintemplates/adminmenubardetail.html'
    queryset = Menubar.objects.all().order_by('-id')
    context_object_name = 'menubar'


class AdminSlideShowListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowlist.html'
    queryset = SlideShow.objects.filter(published=1).order_by('-priority')
    context_object_name = 'slideshows'


class AdminSlideShowCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowcreate.html'
    form_class = SlideShowForm
    success_url = _url_admin + 'slideshow/list/'


class AdminSlideShowUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowcreate.html'
    form_class = SlideShowForm
    model = SlideShow
    success_url = _url_admin + 'slideshow/list/'


class AdminSlideShowDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminslideshowdelete.html'
    model = SlideShow
    success_url = _url_admin + 'slideshow/list/'


class AdminOverviewListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminoverviewlist.html'
    queryset = Overview.objects.all().order_by('-id')
    context_object_name = 'overviews'


class AdminOverviewCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminoverviewcreate.html'
    form_class = OverviewForm
    success_url = _url_admin + 'overview/list/'


class AdminOverviewUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminoverviewcreate.html'
    form_class = OverviewForm
    model = Overview
    success_url = _url_admin + 'overview/list/'


class AdminOverviewDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminoverviewdelete.html'
    model = Overview
    success_url = _url_admin + 'overview/list/'


class AdminSubscriberListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsubscriberlist.html'
    queryset = Subscriber.objects.all().order_by('-id')
    context_object_name = 'subscriberlist'


class AdminSubscriberDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsubscriberdelete.html'
    model = Subscriber
    success_url = _url_admin + 'subscriber/list/'


class AdminPersonCatListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncatlist.html'
    queryset = PersonCat.objects.all()
    context_object_name = 'settings/'


class AdminPersonCatCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncatcreate.html'
    form_class = PersonCatForm
    success_url = _url_admin + 'settings/'


class AdminPersonCatUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncatcreate.html'
    form_class = PersonCatForm
    model = PersonCat
    success_url = _url_admin + 'settings/'


class AdminPersonCatDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/adminpersoncatdelete.html'
    model = PersonCat
    success_url = _url_admin + 'settings/'



class AdminInterestedListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/admininterestedlist.html'
    queryset = Interested.objects.all().order_by('-id')
    context_object_name = 'interesteds'


class AdminInterestedCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/admininterestedcreate.html'
    model = Interested
    form_class = InterestedForm
    success_url = _url_admin + 'interested/list/'


class AdminInterestedUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/admininterestedcreate.html'
    form_class = InterestedForm
    model = Interested
    success_url = _url_admin + 'interested/list/'


class AdminInterestedDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    login_url = '/login/'
    template_name = 'admintemplates/admininteresteddelete.html'
    model = Interested
    success_url = _url_admin + 'interested/list/'


class AdminInterestedDetailView(LoginRequiredMixin, AdminBaseMixin, DetailView):
    login_url = '/login/'
    template_name = 'admintemplates/adminprojectareadetail.html'
    queryset = Interested.objects.all().order_by('-id')
    context_object_name = 'interested'


class AdminSiteSettingListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsitesettinglist.html'
    queryset = SiteSetting.objects.all().order_by('-id')
    context_object_name = 'sitesettings'


class AdminSiteSettingCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsitesettingcreate.html'
    form_class = SiteSettingForm
    success_url = _url_admin + 'settings/'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminSiteSettingUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsitesettingcreate.html'
    model = SiteSetting
    form_class = SiteSettingForm
    success_url = _url_admin + 'sitesetting/list/'


class AdminSiteSettingUpdateUView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminsitesettingcreateU.html'
    model = SiteSetting
    form_class = SiteSettingUForm
    success_url = _url_admin + 'settings/'


class AdminSiteSettingDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    template_name = 'admintemplates/adminsitesettingdelete.html'
    model = SiteSetting
    success_url = _url_admin + 'settings/'

class AdminTestimonialListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/admintestimoniallist.html'
    queryset = Testimonial.objects.all().order_by('-id')
    context_object_name = 'testimonials'


class AdminTestimonialCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/admintestimonialcreate.html'
    form_class = TestimonialForm
    success_url = _url_admin + 'testimonial/list/'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminTestimonialUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/admintestimonialcreate.html'
    model = Testimonial
    form_class = TestimonialForm
    success_url = _url_admin + 'testimonial/list/'


class AdminTestimonialDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    template_name = 'admintemplates/admintestimonialdelete.html'
    model = Testimonial
    success_url = _url_admin + 'testimonial/list/'

class AdminReportCatListView(LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportcatlist.html'
    queryset = ReportCat.objects.all().order_by('-id')
    context_object_name = 'reportcats'


class AdminReportCatCreateView(LoginRequiredMixin, AdminBaseMixin, CreateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportcatcreate.html'
    form_class = ReportCatForm
    success_url = _url_admin + 'settings/'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class AdminReportCatUpdateView(LoginRequiredMixin, AdminBaseMixin, UpdateView):
    login_url = '/login/'
    template_name = 'admintemplates/adminreportcatcreate.html'
    model = ReportCat
    form_class = ReportCatForm
    success_url = _url_admin + 'settings/'


class AdminReportCatDeleteView(LoginRequiredMixin, AdminBaseMixin, DeleteView):
    template_name = 'admintemplates/adminreportcatdelete.html'
    model = ReportCat
    success_url = _url_admin + 'settings/'
#######################Clients################

# class clientNewseventDetailView(BaseMixin, DetailView):
#     template_name = 'clienttemplates/clientnewseventdetail.html'
#     queryset = Newsevent.objects.all().order_by('-id')
#     context_object_name = 'newseventdetails'


# class ClientRecentNewsListView(ListView):
#     template_name = 'clienttemplates/clientnewseventlist.html'
#     queryset = Newsevent.objects.all().order_by('-id')
#     context_object_name = 'newseventlists'


# class CLientGalleryListView(ListView):
#     template_name = 'clienttemplates/clientgallerylist.html'
#     queryset = Gallery.objects.all().order_by('-id')
#     context_object_name = 'gallerylist'

    

class BlogListView(BaseMixin, ListView):
    template_name = 'clienttemplates/blog.html'
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = 'blogs'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newseventlists'] = Newsevent.objects.all().order_by('-id')[:5]
        context['bloglists'] = Blog.objects.all().order_by('-id')[5:8]

        return context


class BlogDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/blogdetail.html'
    queryset = Blog.objects.all().order_by('-id')
    context_object_name = 'blog'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bloglists'] = Blog.objects.all().order_by('-id')[:8]
        context['galleryset'] = Gallery.objects.filter(menubar="2")

        return context


class ClientHomeView(BaseMixin, TemplateView):
    template_name = 'clienttemplates/clienthome.html'


class ClientMenubarView(MenubarMixin, TemplateView):
    template_name = 'clienttemplates/base.html'


class ContactView(BaseMixin, CreateView):
    template_name = 'clienttemplates/contact.html'  
    form_class = SubscriberForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['contactoverviews'] = Overview.objects.filter(published=True)

        return context
    

class ClientGalleryListView(BaseMixin, ListView):
    template_name = 'clienttemplates/gallery.html'
    queryset = Gallery.objects.all().order_by('-id')
    context_object_name = 'galleries'
    paginate_by = 5


class OverviewDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/overviewdetail.html'
    model = Overview
    context_object_name = 'overview'


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bloglists'] = Blog.objects.all().order_by('-id')[:4]
        context['overviewlists'] = Overview.objects.all().order_by('-id')[:4]

        return context


class AboutDetailView(BaseMixin, DetailView):
    template_name = 'clienttemplates/aboutusdetail.html'
    model = Aboutus
    context_object_name = "aboutus"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newseventlists'] = Newsevent.objects.all().order_by('-id')[:2]
        context['bloglists'] = Blog.objects.all().order_by('-id')[:2]

        return context


class NewsEventListView(BaseMixin, ListView):
    template_name ="clienttemplates/newsevent.html"
    queryset = Newsevent.objects.all().order_by('-id')
    context_object_name = 'newsevents'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['overview'] = Overview.objects.all().order_by('-id')
        context['overviews'] = Overview.objects.all()



        return context


class NewsEventDetailView(BaseMixin, DetailView):
    template_name ="clienttemplates/newseventdetail.html"
    model = Newsevent
    context_object_name = "newseventdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['bloglists'] = Blog.objects.all().order_by('-id')[:8]

        return context


class MessageCreateView(BaseMixin, CreateView):
    template_name = 'clienttemplates/contact.html'
    form_class = MessageForm
    success_url = "/"

# class SubscriberCreateView(BaseMixin, CreateView):
#     login_url = '/login/'
#     template_name = 'clienttemplates/contact.html'
#     form_class = SubscriberForm
#     success_url = "/"

class PersonListView(BaseMixin, LoginRequiredMixin, AdminBaseMixin, ListView):
    login_url = '/login/'
    template_name ="clienttemplates/ourteam.html"
    queryset = PersonCat.objects.all().order_by('priority')
    context_object_name = 'ourteamCats'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['overviews'] = Overview.objects.all()

        return context  

class ReportListView(BaseMixin, ListView):
    template_name ="clienttemplates/report.html"
    queryset = ReportCat.objects.all().order_by('priority')
    context_object_name = 'reportcats'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['overviews'] = Overview.objects.all()

        return context 

class ProjectAreaDetailView(BaseMixin, DetailView):
    template_name ="clienttemplates/projectareadetail.html"
    model = ProjectArea
    context_object_name = "projectareadetail"


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['projectarealists'] = ProjectArea.objects.all().order_by('-id')[:5]
        context['bloglists'] = Blog.objects.all().order_by('-id')[:4]


        return context 