from django import forms
from .models import *
from django_summernote.widgets import SummernoteWidget


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class ContactForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = '__all__'

class UserinfoForm(forms.ModelForm):

    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'User Name'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'User Name'}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'User Name'}))

    class Meta:
        model = Userinfo
        fields = ["username", "password", "confirm_password", "full_name",
                  "company_name", "address", "phone", "image","mobile", "email", "website","about"]
        widgets = {
            'full_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Full Name'
            }),
            'company_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Company Name'
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Address'
            }),
            'phone': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Contact Number'
            }),
            'mobile': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Mobile Number'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }),
            'website': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Website'
            }),
            'about': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Website'
            }),
            'about': SummernoteWidget(),      
        }

    def clean_username(self):
        uname = self.cleaned_data["username"]
        if User.objects.filter(username=uname).exists():
            raise forms.ValidationError("This username is already taken")

        return uname

    def clean_confirm_password(self):
        pword = self.cleaned_data["password"]
        c_pword = self.cleaned_data["confirm_password"]
        if pword != c_pword:
            raise forms.ValidationError("Password didnot match")

        return c_pword

class SiteSettingForm(forms.ModelForm):
    class Meta:
        model = SiteSetting
        fields = ['name','value','sitegroup','published']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Site Setting Name'
            }),
            'value': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Values'
            }),
            'sitegroup': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Group'
            }),
            'published': forms.CheckboxInput(attrs={
                'class': 'checkbox11',
            }),
        }

class SiteSettingUForm(forms.ModelForm):
    class Meta:
        model = SiteSetting
        fields = ['name','value','sitegroup','published']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Site Setting Name'
            }),
            'value': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Values'
            }),
            'sitegroup': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Group'
            }),
            'published': forms.CheckboxInput(attrs={
                'class': 'checkbox11',
            }),
        }

        def __init__(self, *args, **kwargs):
            super(SiteSetting, self).__init__(*args, **kwargs)
            instance = getattr(self, 'instance', None)
            if instance and instance.pk:
                self.fields['name'].widget.attrs['readonly'] = True

            
class NoticeForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = ['title', 'content']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Notice title ...'
            }),
            'content': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Notice content ...'
            }),
        }


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ['title', 'image', 'content', 'date_of_event', 'venue']

class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['name', 'email', 'subject', 'phone']
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Name'
            }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email'
            }),
            'subject': forms.TextInput(attrs={
                'placeholder': 'Subject '
            }),
            'phone': forms.TextInput(attrs={
                'placeholder': 'Phone Number'
            }),
            'text': forms.Textarea(attrs={
                'class':"",
                'placeholder': 'Message'
            }),
        }


class TestimonialForm(forms.ModelForm):
    class Meta:
        model = Testimonial
        fields = ['name', 'email', 'address','image', 'workat','text']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Name'
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Address'
            }),
            'workat': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Campay name'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'Image'
            }),
            'text': SummernoteWidget(),            
        }


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['name', 'email', 'subject', 'phone','text']
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Name'
            }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email'
            }),
            'subject': forms.TextInput(attrs={
                'placeholder': 'Subject '
            }),
            'phone': forms.TextInput(attrs={
                'placeholder': 'Phone Number'
            }),
            'text': forms.Textarea(attrs={
                'placeholder': 'Message'
            }),
        }


class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ['title','subtle','text', 'image', 'author']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Blog title ...'
            }),
            'subtle': SummernoteWidget(),
            'text': SummernoteWidget(),
            'author': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'author ...'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'image ...'
            }),
        }


class LogoForm(forms.ModelForm):
    class Meta:
        model = Logo
        fields = ['title', 'alt', 'image']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Title',
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': "Image insert ..."
            }),
            'alt': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "alt text ..."
            }),
            }


class GalleryForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['title', 'caption', 'image', 'published', 'priority', 'menubar']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Title',
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': "Image insert ..."
            }),
            'caption': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Image caption ..."
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Image priority"
            }),
            'menubar': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': "Menu Item"
            }),

        }



class ReportCatForm(forms.ModelForm):
    class Meta:
        model = ReportCat
        fields = ['title', 'priority']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Title',
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Priority Order"
            }),
          
        }

class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['title', 'caption', 'priority','r_type', 'image','file']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Title',
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': "Image insert ..."
            }),
            'caption': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Image caption ..."
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "Image priority"
            }),
            'r_type': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': "Report Types"
            }),
            'file': forms.FileInput(attrs={
                'class': 'form-control',
            }),
 
        }


class AboutusForm(forms.ModelForm):
    class Meta:
        model = Aboutus
        fields = ['title', 'subtitle', 'slug', 'subtle','image', 'caption', 'text']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Title'
            }),
            'subtle':SummernoteWidget(),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter image'
            }),
            'subtitle': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Subtitle..'
            }),
            'caption': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Image caption'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Slug '
            }),
            'text': SummernoteWidget(),            
         }


class NewseventForm(forms.ModelForm):
    class Meta:
        model = Newsevent
        fields = ['title', 'category', 'subtitle','thumbnail_limit','subtle',
                  'priority', 'image', 'text', 'published']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'subtitle': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Subtitle ...'
            }),
            'thumbnail_limit': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Subtle limit'
            }),
            'subtle':SummernoteWidget(),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Priority'
            }),
            'text': SummernoteWidget(),
            'category': forms.Select(attrs={
                'class': 'form-control1',
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control'
            }),
        }


class ProjectAreaForm(forms.ModelForm):
    class Meta:
        model = ProjectArea
        fields = ['title', 'subtitle', 'subtle', 'priority', 'image', 'text']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'subtitle': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Subtitle'
            }),
            'subtle': SummernoteWidget(),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Priority'
            }),
            'text': SummernoteWidget(),
        }

class PersonCatForm(forms.ModelForm):
    class Meta:
        model = PersonCat
        fields = ['title','priority']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Priority order'
            }),
            }

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ['name', 'post', 'address', 'email', 'phone', 'nationality','image','category','priority','text']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Name'
            }),
            'post': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Post'
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'address'
            }),
            'phone': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Phone number'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control',
            }),
            'nationality': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nationality'
            }),
            'category': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Category'
            }),
            'priority': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Priority'
            }),
            'text': SummernoteWidget(),
        }


class InterestedForm(forms.ModelForm):
    class Meta:
        model = Interested
        fields = ['name', 'address', 'email', 'phone', 'amount','country','text']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Name'
            }),
            'email': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Email'
            }),
            'address': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'address'
            }),
            'phone': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Phone number'
            }),
            'country': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Nationality'
            }),
            'amount': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Interested Amount to donate in dollar($)',
                'label':'Amount($)',
            }),
            'text': SummernoteWidget(),
        }


class MenubarForm(forms.ModelForm):
    class Meta:
        model = Menubar
        fields = ['title',
                  'slug', 'link', 'root', 'icon', 'priority', 'published']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'slug  ..'
            }),
            'link': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': ' url link  ..'
            }),
            'icon': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'icon (awesome font v3.2) ..'
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'root': forms.Select(attrs={
                'class': 'form-control'
            }),
            'published': forms.CheckboxInput(attrs={
                'class': 'checkbox11'
            }),
        }


class SlideShowForm(forms.ModelForm):
    class Meta:
        model = SlideShow
        fields = ['title', 'priority', 'caption', 'image', 'published']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Title'
            }),
            'priority': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Priority'
            }),
            'caption': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Main Text here'
            }),
            'image': forms.FileInput(attrs={
                'class': 'form-control'
            }),
            'published': forms.CheckboxInput(attrs={
                'class': 'checkbox11'
            }),
        }


class OverviewForm(forms.ModelForm):
    class Meta:
        model = Overview
        fields = ['title', 'subtitle', 'menubar', 'text','published']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Overview Title ..'
            }),
            'subtitle': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Subtitle'
            }),
            'menubar': forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Category ..',
            }),
            'text': SummernoteWidget(),
        }
