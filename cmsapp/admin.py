from django.contrib import admin
from .models import *


class BlogModelAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug':['title']}

admin.site.register([Message, Event, Notice, Blog,
                     Aboutus, Gallery, SlideShow, Newsevent, ProjectArea, Menubar, MenuCat, Overview])
