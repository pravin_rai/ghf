# Generated by Django 2.2.3 on 2019-08-01 05:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsapp', '0008_auto_20190801_1110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='slug',
            field=models.SlugField(allow_unicode=True, max_length=150, unique=True),
        ),
    ]
